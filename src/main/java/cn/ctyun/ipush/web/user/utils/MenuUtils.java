package cn.ctyun.ipush.web.user.utils;

import cn.ctyun.ipush.web.user.bo.ConsoleUserModuleBO;
import cn.ctyun.ipush.web.user.bo.MenuTreeNode;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: yangya
 * Date: 15-7-8
 * Time: 下午3:55
 * To change this template use File | Settings | File Templates.
 */
public class MenuUtils {

    public static MenuTreeNode gennrateMenuTree(List<Map> moduleBOs){
        // 节点列表（散列表，用于临时存储节点对象）
        HashMap<String,MenuTreeNode> nodeMap = new HashMap<String,MenuTreeNode>();
        // 根节点
        MenuTreeNode root = new MenuTreeNode();
        Map<String, List> btnRight = root.getBtnRight();
        // 根据结果集构造节点列表（存入散列表）
        for (Map consoleUserModuleBO:moduleBOs) {
            MenuTreeNode node = new MenuTreeNode();
            String parentId = (String)consoleUserModuleBO.get("parentId");
            String moduleCode = (String)consoleUserModuleBO.get("moduleCode");
            String fkModuleId = (String) consoleUserModuleBO.get("fkModuleId");
            String moduleName = (String) consoleUserModuleBO.get("moduleName");
            String moduleUrl = (String) consoleUserModuleBO.get("moduleUrl");
            node.setId(fkModuleId);
            node.setName(moduleName);
            node.setParentId(parentId);
            node.setUrl(moduleUrl);
            //去掉按钮类型的数据
            if ("3".equals(consoleUserModuleBO.get("moduleType"))){
                if (btnRight.containsKey(parentId)){
                    btnRight.get(parentId).add(moduleCode);
                }else{
                    List moduleCodeList = new ArrayList();
                    moduleCodeList.add(moduleCode);
                    btnRight.put(parentId, moduleCodeList);
                }
            }
            nodeMap.put(node.getId(), node);
        }
        //将分散的节点串接起来
        for (MenuTreeNode node:nodeMap.values()){
            String parentId = node.getParentId();
            if (parentId ==null||"".equals(parentId)||"-1".equals(parentId)||nodeMap.get(parentId)==null){
                root.addChildren(node);
            }else{
                nodeMap.get(parentId).addChildren(node);
            }
        }

        return root;
    }
}
