package cn.ctyun.ipush.web.user.bo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/5/26.
 */
public class SystemUserPageBo {
    private int pageNo;
    private int pageSize;
    private SystemUserBO query = new SystemUserBO();

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public SystemUserBO getQuery() {
        return query;
    }

    public void setQuery(SystemUserBO query) {
        this.query = query;
    }
}
