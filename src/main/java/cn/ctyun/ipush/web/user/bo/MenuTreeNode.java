package cn.ctyun.ipush.web.user.bo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: yangya
 * Date: 15-7-7
 * Time: 下午5:54
 * To change this template use File | Settings | File Templates.
 */
public class MenuTreeNode implements Serializable {
    private String id;
    private String name;
    private String url;
    private String parentId;
    private Integer level;

    private List<MenuTreeNode> children = new ArrayList<MenuTreeNode>();
    private Map<String,List> btnRight = new HashMap<String,List>();

    public String getId() {
        return id;
    }
    public void setId(String id) {
    this.id = id;
}

    public String getName() {
    return name;
}

    public void setName(String name) {
    this.name = name;
}

    public String getUrl() {
    return url;
}

    public void setUrl(String url) {
    this.url = url;
}

    public String getParentId() {
    return parentId;
}

    public void setParentId(String parentId) {
    this.parentId = parentId;
}

    public Integer getLevel() {
    return level;
}

    public void setLevel(Integer level) {
    this.level = level;
}
    public List<MenuTreeNode> getChildren() {
        return children;
    }

    public boolean isChildOf(String parentId){
        if (parentId == null){
            return false;
        }
        return parentId.equals(this.parentId);
    }

    public void addChildren(MenuTreeNode node) {
        this.children.add(node);
    }

    public Map<String,List> getBtnRight() {
        return btnRight;
    }

    public void setBtnRight(Map btnRight) {
        this.btnRight = btnRight;
    }
}
