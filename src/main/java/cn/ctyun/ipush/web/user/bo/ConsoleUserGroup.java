package cn.ctyun.ipush.web.user.bo;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by Administrator on 2015/5/26.
 */
public class ConsoleUserGroup {

    private String id;
    private String name;
    private Integer sort;
    private String description;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateDate;
    private Integer status;
    private String code;

    public ConsoleUserGroup(){}
    public ConsoleUserGroup(LinkedHashMap map){
            setCreateUser((String)map.get("createUser"));
            setDescription((String) map.get("description"));
            setId((String)map.get("id"));
            setName((String)map.get("name"));
            setSort((Integer)map.get("sort"));
            setStatus((Integer)map.get("status"));
            setUpdateUser((String)map.get("updateUser"));
            setCode((String)map.get("code"));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
