package cn.ctyun.ipush.web.user.bo;

import java.util.Date;

public class ConsoleUserModuleBO {
	    private String id;
	    private String fkUserId; //菜单名
	    private String fkModuleId;  //链接地址
	    private String description;  //模块编码（逻辑组件)
	    private String createUser;
	    private Date createDate;
	    private String updateUser;
	    private Date updateDate;
	    private Integer status;
	    private String moduleName; //菜单名
	    private String moduleUrl;  //链接地址
	    private String moduleCode;  //模块编码（逻辑组件)
	    private Integer moduleDeep; //深度(1级模块、2级模块、3级模块)
	    private String modulePath;  //模块编码全路径“/”隔开查询下级全模块时用(编码1/编码2/编码3)
	    private Integer moduleSort; //排序
	    private Integer moduleType; //类型（1、导航模块 2、页面模块 3、功能模块）
		private String moduleDesc; //描述
	    private String parentId;

		public String getModuleName() {
			return moduleName;
		}
		public void setModuleName(String moduleName) {
			this.moduleName = moduleName;
		}
		public String getModuleUrl() {
			return moduleUrl;
		}
		public void setModuleUrl(String moduleUrl) {
			this.moduleUrl = moduleUrl;
		}
		public String getModuleCode() {
			return moduleCode;
		}
		public void setModuleCode(String moduleCode) {
			this.moduleCode = moduleCode;
		}
		public Integer getModuleDeep() {
			return moduleDeep;
		}
		public void setModuleDeep(Integer moduleDeep) {
			this.moduleDeep = moduleDeep;
		}
		public String getModulePath() {
			return modulePath;
		}
		public void setModulePath(String modulePath) {
			this.modulePath = modulePath;
		}
		public Integer getModuleSort() {
			return moduleSort;
		}
		public void setModuleSort(Integer moduleSort) {
			this.moduleSort = moduleSort;
		}
		public Integer getModuleType() {
			return moduleType;
		}
		public void setModuleType(Integer moduleType) {
			this.moduleType = moduleType;
		}
		public String getModuleDesc() {
			return moduleDesc;
		}
		public void setModuleDesc(String moduleDesc) {
			this.moduleDesc = moduleDesc;
		}
		public String getParentId() {
			return parentId;
		}
		public void setParentId(String parentId) {
			this.parentId = parentId;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getFkUserId() {
			return fkUserId;
		}
		public void setFkUserId(String fkUserId) {
			this.fkUserId = fkUserId;
		}
		public String getFkModuleId() {
			return fkModuleId;
		}
		public void setFkModuleId(String fkModuleId) {
			this.fkModuleId = fkModuleId;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getCreateUser() {
			return createUser;
		}
		public void setCreateUser(String createUser) {
			this.createUser = createUser;
		}
		public Date getCreateDate() {
			return createDate;
		}
		public void setCreateDate(Date createDate) {
			this.createDate = createDate;
		}
		public String getUpdateUser() {
			return updateUser;
		}
		public void setUpdateUser(String updateUser) {
			this.updateUser = updateUser;
		}
		public Date getUpdateDate() {
			return updateDate;
		}
		public void setUpdateDate(Date updateDate) {
			this.updateDate = updateDate;
		}
		public Integer getStatus() {
			return status;
		}
		public void setStatus(Integer status) {
			this.status = status;
		}
}
