package cn.ctyun.ipush.web.user.bo;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * Created by Administrator on 2015/5/26.
 */
public class ConsoleDepartmentBo {
    private String id;
    private String departmentName;
    private String departmentCode;
    private String parentId;
    private Integer departmentType;
    private String departmentPath;
    private String createUser;
    private Date createDate;
    private String updateUser;
    private Date updateDate;
    private Integer status;

    public ConsoleDepartmentBo(){}

    public ConsoleDepartmentBo(LinkedHashMap map){
        setId((String)map.get("id"));
        setDepartmentName((String)map.get("departmentName"));
        setDepartmentCode((String)map.get("departmentCode"));
        try{
            setDepartmentType(Integer.parseInt(map.get("departmentType")+""));
        }catch(Exception e)
        {

        }
        setParentId((String)map.get("parentId"));
        setDepartmentPath((String)map.get("departmentPath"));
        setCreateUser((String)map.get("createUser"));

        setUpdateUser((String)map.get("updateUser"));


        setStatus((Integer)map.get("status"));
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(Integer departmentType) {
        this.departmentType = departmentType;
    }

    public String getDepartmentPath() {
        return departmentPath;
    }

    public void setDepartmentPath(String departmentPath) {
        this.departmentPath = departmentPath;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
