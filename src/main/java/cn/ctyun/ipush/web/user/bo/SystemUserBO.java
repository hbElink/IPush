package cn.ctyun.ipush.web.user.bo;

import java.util.Date;
import java.util.LinkedHashMap;

/**
 * 用户表
 * @author Administrator
 *
 */
public class SystemUserBO {
    private String id;

    private String userPhone; //登录手机号

    private String userPassword; //密码

    private String userLoginName; //用户登录名

    private String userSelect;

    private String city; //城市

    private String province; //省份

    private String userRealname; //姓名

    private String userEmail; //邮箱

    private String userJob; //职务

    private String fkUserDepId; //用户所属部门

    private String createUser;

    private Date createDate;

    private String updateUser;

    private Date updateDate;

    private Integer status;

    private String groupName;
    // --------------连接的关联表属性
    private String departmentName;
    private String departmentCode;
    private String departmentType;

    public SystemUserBO(){

    }

    public SystemUserBO(LinkedHashMap map){
        setId((String)map.get("id"));
        setUserPhone((String)map.get("userPhone"));
        setUserPassword((String)map.get("userPassword"));
        setUserLoginName((String)map.get("userLoginName"));
        setCity((String)map.get("city"));
        setProvince((String)map.get("province"));
        setUserRealname((String)map.get("userRealname"));
        setUserEmail((String)map.get("userEmail"));
        setUserJob((String)map.get("userJob"));
        setFkUserDepId((String)map.get("fkUserDepId"));
        setCreateUser((String)map.get("createUser"));
        setUpdateUser((String)map.get("updateUser"));
        setStatus((Integer)map.get("status"));
    }

    public String getDepartmentType() {
        return departmentType;
    }

    public void setDepartmentType(String departmentType) {
        this.departmentType = departmentType;
    }

    private String departmentPath;
    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }


    public String getDepartmentPath() {
        return departmentPath;
    }

    public void setDepartmentPath(String departmentPath) {
        this.departmentPath = departmentPath;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone == null ? null : userPhone.trim();
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword == null ? null : userPassword.trim();
    }
    public String getUserLoginName() {
        return userLoginName;
    }

    public void setUserLoginName(String userLoginName) {
        this.userLoginName = userLoginName == null ? null : userLoginName.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getUserRealname() {
        return userRealname;
    }

    public void setUserRealname(String userRealname) {
        this.userRealname = userRealname == null ? null : userRealname.trim();
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail == null ? null : userEmail.trim();
    }

    public String getUserJob() {
        return userJob;
    }

    public void setUserJob(String userJob) {
        this.userJob = userJob == null ? null : userJob.trim();
    }

    public String getFkUserDepId() {
        return fkUserDepId;
    }

    public void setFkUserDepId(String fkUserDepId) {
        this.fkUserDepId = fkUserDepId == null ? null : fkUserDepId.trim();
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser == null ? null : createUser.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser == null ? null : updateUser.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getUserSelect() {
        return userSelect;
    }

    public void setUserSelect(String userSelect) {
        this.userSelect = userSelect;
    }
}