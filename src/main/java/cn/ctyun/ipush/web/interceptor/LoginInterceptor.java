package cn.ctyun.ipush.web.interceptor;

import cn.ctyun.ipush.web.constanst.*;
import cn.ctyun.ipush.web.user.bo.SystemUserBO;
import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 2015/5/21.
 */
public class LoginInterceptor implements HandlerInterceptor {

    private final Logger log = Logger.getLogger(LoginInterceptor.class);
    public String[] allowUrls;//还没发现可以直接配置不拦截的资源，所以在代码里面来排除
    public String mappingURL;
    public String redirectURL;
    public String apiURL;

    public void setApiURL(String apiURL) {
        this.apiURL = apiURL;
    }

    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    public void setMappingURL(String mappingURL) {
        this.mappingURL = mappingURL;
    }

    public void setAllowUrls(String[] allowUrls) {
        this.allowUrls = allowUrls;
    }
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String url = request.getRequestURI();
        boolean flag = false;
        SystemUserBO user =null;
        //允许的url不进行拦截,主要是静态资源
        for(String allow:allowUrls){
            if(url.contains(allow)){
                return  true;  //true  继续后面的拦截器和cotroller  false 返回，不继续后面的处理
            }
        }
        if(url!=null){
            if(url.toLowerCase().matches(mappingURL))   {  //mappingURL 表示 .*login*
                log.info("====url===="+url+"===== connection");
                flag = true;
            }else{
                if(request.getSession().getAttribute(SessionAttributeKeys.SESSION_ATTR_KEY_USERINFO)==null){
                    flag = false;
                }else{
                    try {
                        user = (SystemUserBO) request.getSession().getAttribute(SessionAttributeKeys.SESSION_ATTR_KEY_USERINFO);
                    }catch (Exception e){
                        user = null;
                    }
                }

                if(null != user){
                    log.info("====url===="+url+"===== connection");
                    flag = true;
                }
            }
        }

        if(!flag){
            //登陆用户失效，重新转到登陆页面
            log.info("====url===="+url+"===== refuse connection");
            //response.sendRedirect(redirectURL);
            /**
             * 判断是不是登录请求，如果不是，没有登录的情况下，并且是api请求，设置status 401
             */

            if(!url.equals("/")&&url.contains("api")){
                response.setStatus(401);
            }else{
                //如果是直接到登录页面
                response.sendRedirect(redirectURL);
            }
            return flag;
        }else{
            return flag;
        }//To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
//        String url = request.getRequestURI();
//        boolean flag = false;
//        if(url!=null&&request.getSession().getAttribute("userInfo")!=null){
//            if(url.toLowerCase().indexOf(apiURL)>-1)   {
//                flag = true;
//            }else{
//                flag = false;
//            }
//        }
//        if(!flag){
//            request.getRequestDispatcher("/pages/index.jsp").forward(request,response);
//        }

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

