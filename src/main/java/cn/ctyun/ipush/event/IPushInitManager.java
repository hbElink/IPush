package cn.ctyun.ipush.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-16
 * Time: 下午3:16
 * To change this template use File | Settings | File Templates.
 */
@Component
public class IPushInitManager {

    /** 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(IPushInitManager.class);

    /** 连接池service */
    @Autowired
    private ConnectionPoolInit connectionPoolInit;

    @Autowired
    private ScheduleJobInit scheduleJobInit;

    @PostConstruct
    public void init(){
        //must first init connetionSink and  then start job
        connectionPoolInit.init();
        scheduleJobInit.init();
    }
}
