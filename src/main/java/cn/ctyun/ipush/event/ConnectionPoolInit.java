package cn.ctyun.ipush.event;

import cn.ctyun.ipush.dao.TaskDao;
import cn.ctyun.ipush.model.TaskModel;
import cn.ctyun.ipush.utils.DBUtils;
import cn.ctyun.ipush.vo.TaskVo;
import com.dexcoder.dal.JdbcDao;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-15
 * Time: 下午6:16
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ConnectionPoolInit {

    /** 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(ConnectionPoolInit.class);


    /**
     * 通用dao
     */
    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private TaskDao taskDao;

    /**
     * 项目启动时初始化
     * 根据task，按 url username password jobname cronTrigger 创建 连接，进行缓存
     */

    public void init() {

        if (LOG.isInfoEnabled()) {
            LOG.info("--- ConnectionPool init----------------");
        }
        long start=System.currentTimeMillis();
        List<TaskModel> taskModelList = jdbcDao.queryList(TaskModel.class);
        if (CollectionUtils.isEmpty(taskModelList)){
            LOG.info("--- --- no exist task  data --");
        }else{
            for (TaskModel taskModel:taskModelList){
                TaskVo taskVo=taskDao.getTaskVoByTaskModel(taskModel);
                for(String cron:taskVo.getTaskDetail().getCronExpression().split("@")){
                    DBUtils.createOracleConnection(taskVo.getDataSource().getDataSourceUrl(),taskVo.getDataSource().getUserName(),taskVo.getDataSource().getPassword(),taskVo.getTaskDetail().getTaskGroupName(),taskVo.getTaskDetail().getTaskName(),cron);
                }
            }
        }
        //connectionPoolService.init();
        long end=System.currentTimeMillis();
        if (LOG.isInfoEnabled()) {
            LOG.info(String.format("----------------ConnectionPool end ,cost time: %d----------------",end-start));
        }
    }
}
