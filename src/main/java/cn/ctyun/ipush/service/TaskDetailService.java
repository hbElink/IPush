package cn.ctyun.ipush.service;

import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.vo.TaskDetailVo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-19
 * Time: 下午2:20
 * To change this template use File | Settings | File Templates.
 */
public interface TaskDetailService {
    public void save(TaskDetailVo taskDetailVo);
    public void save(TaskDetailModel taskDetailModel);

    public int delete(String taskDetailId);

    public int update(TaskDetailVo taskDetailVo);
    public int update(TaskDetailModel taskDetailModel);

    public TaskDetailVo querySingleResultVo(String taskDetailId);
    public TaskDetailModel querySingleResult(String taskDetailId);
    public List<TaskDetailModel> queryList(TaskDetailModel taskDetailModel);
    public List<TaskDetailVo> queryListVo(TaskDetailVo taskDetailVo);
    public boolean isExist(TaskDetailModel taskDetailModel);
}
