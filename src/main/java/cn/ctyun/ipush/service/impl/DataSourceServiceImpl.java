package cn.ctyun.ipush.service.impl;


import cn.ctyun.ipush.dao.DataSourceDao;
import cn.ctyun.ipush.dao.TaskDao;
import cn.ctyun.ipush.dao.TaskDetailDao;
import cn.ctyun.ipush.dto.ConnectionObject;
import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.model.TaskModel;
import cn.ctyun.ipush.service.DataSourceService;
import cn.ctyun.ipush.service.ScheduleJobService;
import cn.ctyun.ipush.utils.DBUtils;
import cn.ctyun.ipush.utils.DateUtils;
import cn.ctyun.ipush.utils.ScheduleUtils;
import cn.ctyun.ipush.vo.DataSourceVo;
import com.dexcoder.commons.bean.BeanConverter;
import com.dexcoder.commons.utils.UUIDUtils;
import com.dexcoder.dal.build.Criteria;
import com.dexcoder.dal.spring.JdbcDaoImpl;
import org.apache.commons.lang.StringUtils;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLException;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-11
 * Time: 下午5:16
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DataSourceServiceImpl implements DataSourceService {

    /* 日志对象 */
    private final Logger LOG = LoggerFactory.getLogger(DataSourceServiceImpl.class);

    @Autowired
    private DataSourceDao dataSourceDao;

    @Autowired
    private TaskDao taskDao;


    @Autowired
    private TaskDetailDao taskDetailDao;

    @Autowired
    private ScheduleJobService scheduleJobService;

    /**
     * 数据源
     *
     * @param dataSourceVo
     */
    @Override
    @Transactional
    public void save(DataSourceVo dataSourceVo) {
        dataSourceVo.setDataSourceModelId(UUIDUtils.getUUID32());
        DataSourceModel dataSourceModel = dataSourceVo.getTargetObject(DataSourceModel.class);
        //保存到数据库中
        save(dataSourceModel);
    }

    @Override
    @Transactional
    public void save(DataSourceModel dataSourceModel) {
        dataSourceDao.save(dataSourceModel);
    }

    @Override
    @Transactional
    public int delete(String dataSourceId) {
        //判断是否存在引用，是否在task中.如果有引用肯定存在Job，此时不删除；
        if(taskDao.isExistDataSourceInTask(dataSourceId)){
            return 0;
        }
        //不存在，删除缓存，数据库记录删除；无引用肯定也不存在对应的Job
        DBUtils.deleteConnectionCache(dataSourceDao.querySingleResult(dataSourceId));
        //数据库记录删除
        return dataSourceDao.delete(dataSourceId);
    }

    @Override
    @Transactional
    public int update(DataSourceVo dataSourceVo) {
        if(StringUtils.isEmpty(dataSourceVo.getDataSourceModelId())){
            throw new RuntimeException("dataSource 主键ID 不存在");
        }
        //todo 额外处理
        DataSourceModel dataSourceModel = dataSourceVo.getTargetObject(DataSourceModel.class);
        return dataSourceDao.update(dataSourceModel);
    }

    @Override
    @Transactional
    public int update(DataSourceModel dataSourceModel) {
//        String username = dataSourceModel.getUserName();
//        String password = dataSourceModel.getPassword();
//        String url = dataSourceModel.getDataSourceUrl();
//        DataSourceModel dsm_db = querySingleResult(dataSourceModel.getDataSourceModelId());
//        String dbUsername = dsm_db.getUserName();
//        String dbDataSourceUrl = dsm_db.getDataSourceUrl();
//        String dbPassword = dsm_db.getPassword();
//        if (dbDataSourceUrl != url || dbUsername != username || dbPassword != password) {
//            LOG.info("---更新缓存开始---");
//            //判断缓存是否有效，是否占用,
//            List<TaskDetailModel> taskDetailModelList = getTaskDetailModelByDataSourceId(dataSourceModel.getDataSourceModelId());
//            //一个连接可能对应多个Task，一个task可能对应多个连接对象
//            Map<String, List<String>> connDbUseRecord = new HashMap<String, List<String>>();//如果都没占用在删除该连接缓存，在创建。
//            Map<String, List<String>> connNewUseRecord = new HashMap<String, List<String>>();//如果都没占用在删除该连接缓存，在创建。
//            boolean isCanDeleted = true;
//            for (TaskDetailModel taskDetailModel : taskDetailModelList) {
//                Map<String, CronTrigger> cronMap = new HashMap<String, CronTrigger>();
//                for (String cron : taskDetailModel.getCronExpression().split("@")) {
//                    String cronTrigger = ScheduleUtils.trigerNameFormat(taskDetailModel.getTaskName(), cron);
//                    String groupName = taskDetailModel.getTaskGroupName();
//                    String taskName = taskDetailModel.getTaskName();
//                    String connectionDbDesc = DBUtils.getConnnectionIdentity(dbDataSourceUrl, dbUsername, dbPassword, groupName, taskName, cronTrigger);
//                    String connectionNewDesc = DBUtils.getConnnectionIdentity(url, username, password, groupName, taskName, cronTrigger);
//                    //是否存在该连接字符串
//                    if (DBUtils.connectionCacheMap.containsKey(connectionDbDesc)) {
//                        ConnectionObject connectionObject = (ConnectionObject) DBUtils.connectionCacheMap.get(connectionDbDesc);
//                        //如果占用 则提示失败
//                        if (connectionObject.getIsUsed()) {
//                            LOG.info("---Warn: 此连接正在使用，请稍后修改");
//                            isCanDeleted = false;
//                            break;
//                        } else {
//                            connDbUseRecord.put(connectionDbDesc, new ArrayList(Arrays.asList(dbDataSourceUrl, dbUsername, dbPassword, groupName, taskName, cronTrigger)));//1 代表在用 0代表空闲
//                            connNewUseRecord.put(connectionNewDesc, new ArrayList(Arrays.asList(url, username, password, groupName, taskName, cronTrigger)));//1 代表在用 0代表空闲
//                        }
//                    } else {
//                        LOG.info("---Error: 此连接不存在");
//                    }
//                }
//                if (!isCanDeleted) {
//                    break;
//                }
//            }
//            //可以刪除
//            if (isCanDeleted) {
//                //先刪除，再創建
//                for (String connDesc : connDbUseRecord.keySet()) {
//                    ConnectionObject connectionObject = (ConnectionObject) DBUtils.connectionCacheMap.get(connDesc);
//                    //是否连接关闭
//                    try {
//                        if (connectionObject.getConnection() != null && !connectionObject.getConnection().isClosed()) {
//                            connectionObject.getConnection().close();
//                        }
//                        DBUtils.connectionCacheMap.remove(connDesc);
//                    } catch (SQLException e) {
//                        LOG.info(e.getMessage());
//                    }
//                }
//                for (List<String> connValues : connNewUseRecord.values()) {
//                    DBUtils.createOracleConnection(connValues.get(0), connValues.get(1), connValues.get(2), connValues.get(3), connValues.get(4), connValues.get(5));
//                }
//                LOG.info("---正在更新緩存結束---");
//            } else {
//                LOG.info("---update fail---");
//                return 0;
//            }
//        }
        return dataSourceDao.update(dataSourceModel);
    }

    /**
     * 任务cron变化，更新连接池缓存
     * @param taskGroupName
     * @param taskName
     * @param cronExpression 新的cron
     */
    @Override
    @Transactional
    public boolean update(String taskGroupName, String taskName, String cronExpression) {
        //是否可更新

        //是否连接关闭

        return false;

        //To change body of implemented methods use File | Settings | File Templates.
    }

    private List<TaskDetailModel> getTaskDetailModelByDataSourceId(String dataSourceModelId) {
        List<TaskDetailModel> taskDetailModelList = new ArrayList<TaskDetailModel>();
        Criteria criteria = Criteria.select(TaskModel.class).where("dataSourceModelId", new Object[]{dataSourceModelId});
        List<TaskModel> taskModelList = taskDao.queryList(criteria);
        for (TaskModel taskModel : taskModelList) {
            TaskDetailModel taskDetailModel = taskDetailDao.querySingleModel(taskModel.getTaskDetailId());
            taskDetailModelList.add(taskDetailModel);
        }
        return taskDetailModelList;
    }

    @Override
    public DataSourceVo querySingleResultVo(String dataSourceId) {
        DataSourceModel dataSourceModel = querySingleResult(dataSourceId);
        return dataSourceModel.getTargetObject(DataSourceVo.class);
    }

    @Override
    public List<DataSourceVo> queryListVo(DataSourceVo dataSouceVo) {
        List<DataSourceModel> dataSourceModelList = queryList(dataSouceVo.getTargetObject(DataSourceModel.class));
        return BeanConverter.convert(DataSourceVo.class, dataSourceModelList);
    }

    @Override
    public List<DataSourceModel> queryList(DataSourceModel dataSourceModel) {
        return dataSourceDao.queryList(dataSourceModel);
    }

    @Override
    public DataSourceModel querySingleResult(String dataSourceId) {
        return dataSourceDao.querySingleResult(dataSourceId);
    }


}
