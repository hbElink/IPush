package cn.ctyun.ipush.service.impl;

import cn.ctyun.ipush.dao.TaskDetailDao;
import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.service.TaskDetailService;
import cn.ctyun.ipush.vo.TaskDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-19
 * Time: 下午2:20
 * To change this template use File | Settings | File Templates.
 */
@Service("taskDetailService")
public class taskDetailServiceImpl implements TaskDetailService {

    @Autowired
    private TaskDetailDao taskDetailDao;

    public void save(TaskDetailVo taskDetailVo) {

    }

    public void save(TaskDetailModel taskDetailModel) {

    }

    public int delete(String taskDetailId) {
        return taskDetailDao.delele(taskDetailId);
    }

    public int update(TaskDetailVo taskDetailVo) {
        taskDetailDao.update(taskDetailVo);
        return 0;
    }

    public int update(TaskDetailModel taskDetailModel) {
        return 0;
    }

    public TaskDetailVo querySingleResultVo(String taskDetailId) {
        return null;
    }

    public TaskDetailModel querySingleResult(String taskDetailId) {
        return null;

    }

    public List<TaskDetailModel> queryList(TaskDetailModel taskDetailModel) {
        return null;
    }

    public List<TaskDetailVo> queryListVo(TaskDetailVo taskDetailVo) {
        return null;
    }

    public boolean isExist(TaskDetailModel taskDetailModel) {
        return taskDetailDao.isExist(taskDetailModel);
    }
}
