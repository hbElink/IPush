package cn.ctyun.ipush.service;

import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.vo.DataSourceVo;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: wjf
 * Date: 16-6-11
 * Time: 下午5:15
 * To change this template use File | Settings | File Templates.
 */
public interface DataSourceService {

    public void save(DataSourceVo dataSourceVo);
    public void save(DataSourceModel  dataSourceModel);

    public int delete(String dataSourceId);

    public int update(DataSourceVo dataSourceVo);
    public int update(DataSourceModel dataSourceModel);

    public DataSourceVo querySingleResultVo(String dataSourceId);
    public DataSourceModel querySingleResult(String dataSourceId);
    public List<DataSourceModel> queryList(DataSourceModel dataSourceModel);
    public List<DataSourceVo>  queryListVo(DataSourceVo dataSouceVo);

    public boolean update(String taskGroupName, String taskName, String cronExpression);
}
