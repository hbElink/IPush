package cn.ctyun.ipush.service;

import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.vo.ScheduleJobVo;
import cn.ctyun.ipush.vo.TaskVo;

import java.util.List;

/**
 * 定时任务service
 *
 * Created by liyd on 12/19/14.
 */
public interface ScheduleJobService {

    /**
     * 初始化定时任务
     */
    public void initScheduleJob();

    /**
     * 新增
     * 
     * @param taskVo
     * @return
     */
    //wjf
    public boolean insert(TaskVo taskVo);
    public Long insert(ScheduleJobVo scheduleJobVo);

    //wjf
    public boolean update(TaskVo taskVo);

    /**
     * 直接修改 只能修改运行的时间，参数、同异步等无法修改
     * 
     * @param scheduleJobVo
     */
    public void update(ScheduleJobVo scheduleJobVo);

    /**
     * 删除重新创建方式
     * 
     * @param scheduleJobVo
     */
    public void delUpdate(ScheduleJobVo scheduleJobVo);

    /**
     * 删除
     * 
     * @param scheduleJobId
     */
    //public void delete(String scheduleJobId);
    public void delete(Long scheduleJobId);

    public void delete(String taskId);

    /**
     * 运行一次任务
     *
     * @param scheduleJobId the schedule job id
     * @return
     */
    public void runOnce(String scheduleJobId);
    public void runOnce(Long scheduleJobId);
    /**
     * 暂停任务
     *
     * @param scheduleJobId the schedule job id
     * @return
     */
    public void pauseJob(String scheduleJobId);
    public void pauseJob(Long scheduleJobId);

    /**
     * 恢复任务
     *
     * @param scheduleJobId the schedule job id
     * @return
     */
    public void resumeJob(String scheduleJobId);
    public void resumeJob(Long scheduleJobId);


    /**
     * 获取任务对象
     * 
     * @param scheduleJobId
     * @return
     */
    public ScheduleJobVo get(String scheduleJobId);
    public ScheduleJobVo get(Long scheduleJobId);
    /**
     * 查询任务列表
     * 
     * @param scheduleJobVo
     * @return
     */
    public List<ScheduleJobVo> queryList(ScheduleJobVo scheduleJobVo);

    /**
     * 获取运行中的任务列表
     *
     * @return
     */
    public List<ScheduleJobVo> queryExecutingJobList();


    public void updateExeucteJobParamInfo(DataSourceModel dataSourceModel);
}
