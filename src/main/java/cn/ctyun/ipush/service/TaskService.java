package cn.ctyun.ipush.service;

import cn.ctyun.ipush.dto.ResultDto;
import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.vo.TaskVo;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-11
 * Time: 下午4:42
 * To change this template use File | Settings | File Templates.
 */
public interface TaskService {

    public ResultDto save(TaskVo taskVo);

    public ResultDto update(TaskVo taskVo);

    public ResultDto delete(String taskId);

    public TaskVo getTaskVoByTaskId(String taskId);

}
