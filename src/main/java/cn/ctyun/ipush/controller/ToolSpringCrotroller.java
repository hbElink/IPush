package cn.ctyun.ipush.controller;

import cn.ctyun.ipush.event.ToolSpring;
import cn.ctyun.ipush.model.TaskModel;
import com.dexcoder.dal.JdbcDao;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-20
 * Time: 下午12:29
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/tool")
public class ToolSpringCrotroller {

    //无参数的get请求,返回json
    @RequestMapping(value = "testTool", method = RequestMethod.GET)
    @ResponseBody //此处的作用就是转换为JSON格式返回
    public  List<TaskModel> getTaskInfoByGet() {
        TaskModel taskModel = new TaskModel();
        JdbcDao jdbcDao=(JdbcDao)ToolSpring.getApplicationContext().getBean("jdbcDao");
        List<TaskModel> taskModelList=jdbcDao.queryList(taskModel);
        return taskModelList;
    }
}
