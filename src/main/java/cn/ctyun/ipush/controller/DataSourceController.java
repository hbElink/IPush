package cn.ctyun.ipush.controller;

import cn.ctyun.ipush.constant.ResultInfo;
import cn.ctyun.ipush.dto.ResultDto;
import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.service.DataSourceService;
import cn.ctyun.ipush.vo.DataSourceVo;
import cn.ctyun.ipush.vo.TaskVo;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-18
 * Time: 下午12:52
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/dataSource")
public class DataSourceController {

    @Autowired
    private DataSourceService dataSourceService;

    /* 日志对象 */
    private final Logger LOG = LoggerFactory.getLogger(DataSourceController.class);


    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto saveTask(HttpServletRequest request) {
        DataSourceVo dataSoureVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            dataSoureVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", DataSourceVo.class);
        } else {
            dataSoureVo = JSON.parseObject(configParam, DataSourceVo.class);
        }
        rd = new ResultDto();
        try {
            dataSourceService.save(dataSoureVo);
            rd.setStatus(ResultInfo.success).setInfo("save dataSource success");
        } catch (Exception ex) {
            LOG.info("save task failue :"+ex.getMessage());
            rd.setInfo("saveTask failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }


    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto update(HttpServletRequest request) {
        DataSourceVo dataSoureVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            dataSoureVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", DataSourceVo.class);
        } else {
            dataSoureVo = JSON.parseObject(configParam, DataSourceVo.class);
        }
        rd = new ResultDto();
        try {
            int count= dataSourceService.update(dataSoureVo);
            if(1==count){
                rd.setStatus(ResultInfo.success).setInfo("update dataSource succeess");
            }else{
                rd.setStatus(ResultInfo.failure).setInfo("update dataSource failue,return count not right");
            }
        } catch (Exception ex) {
            rd.setInfo("update  dataSource failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }

    @RequestMapping(value = "querySingle", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto querySingle(HttpServletRequest request) {
        DataSourceVo dataSoureVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            dataSoureVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", DataSourceVo.class);
        } else {
            dataSoureVo = JSON.parseObject(configParam, DataSourceVo.class);
        }
        rd = new ResultDto();
        try {
            DataSourceVo returnDataSourceVo= dataSourceService.querySingleResultVo(dataSoureVo.getDataSourceModelId());

            rd.setStatus(ResultInfo.success).setInfo(JSON.toJSONString(returnDataSourceVo));
        } catch (Exception ex) {
            rd.setInfo("query single dataSource failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }


    @RequestMapping(value = "queryList", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto queryList(HttpServletRequest request) {
        DataSourceVo dataSoureVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            dataSoureVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", DataSourceVo.class);
        } else {
            dataSoureVo = JSON.parseObject(configParam, DataSourceVo.class);
        }
        rd = new ResultDto();
        try {
            List<DataSourceVo> dataSourceVoList= dataSourceService.queryListVo(dataSoureVo);
            rd.setStatus(ResultInfo.success).setInfo(dataSourceVoList);
        } catch (Exception ex) {
            rd.setInfo("query mutliple dataSource failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }


    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto delete(HttpServletRequest request) {
        DataSourceVo dataSoureVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            dataSoureVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", DataSourceVo.class);
        } else {
            dataSoureVo = JSON.parseObject(configParam, DataSourceVo.class);
        }
        rd = new ResultDto();
        try {
            int count= dataSourceService.delete(dataSoureVo.getDataSourceModelId());
            if(1==count){
                rd.setStatus(ResultInfo.success).setInfo(" delete dataSource success");
            }else{
                rd.setInfo("delete  dataSource  failue,return count not right").setStatus(ResultInfo.failure);
            }
        } catch (Exception ex) {
            rd.setInfo("delete  dataSource  failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }
}
