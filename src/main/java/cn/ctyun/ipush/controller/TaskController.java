package cn.ctyun.ipush.controller;

import cn.ctyun.ipush.constant.ResultInfo;
import cn.ctyun.ipush.dto.ResultDto;
import cn.ctyun.ipush.model.TaskModel;
import cn.ctyun.ipush.service.TaskService;
import cn.ctyun.ipush.vo.DataSourceVo;
import cn.ctyun.ipush.vo.ResultVo;
import cn.ctyun.ipush.vo.TaskVo;
import com.alibaba.fastjson.JSON;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-13
 * Time: 下午5:17
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("/task")
public class TaskController {

    /* 日志对象 */
    private final Logger LOG = LoggerFactory.getLogger(TaskController.class);

    @Autowired
    private TaskService taskService;


    //无参数的get请求,返回json
    @RequestMapping(value = "getTaskInfo", method = RequestMethod.GET)
    @ResponseBody //此处的作用就是转换为JSON格式返回
    public TaskModel getTaskInfoByGet() {
        TaskModel taskModel = new TaskModel();
        taskModel.setDataSourceId("1");
        taskModel.setExcelId("1");
        taskModel.setEmailId("1");
        return taskModel;
    }

    //含参数的get请求,返回json
    @RequestMapping(value = "getTaskInfo/{taskId}", method = RequestMethod.GET)
    @ResponseBody
    public TaskModel getTaskInfoByTaskId(@PathVariable String taskId) {
        TaskModel taskModel = new TaskModel();
        taskModel.setDataSourceId("1");
        taskModel.setExcelId("1");
        taskModel.setEmailId("1");
        return taskModel;
    }

    //含参数的get请求,返回视图
    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String getTaskInfoByTaskId() {
        return "input-schedule-job";
    }

    //含参数的post请求
    @RequestMapping(value = "saveTask", method = RequestMethod.POST)
    @ResponseBody
    public String getTaskInfoByPost(HttpServletRequest request) {
        String username = request.getParameter("task").trim();
        System.out.println(username);
        return username;
    }



    //**----------------------------------------------------------------
    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto save(HttpServletRequest request) {
        TaskVo taskVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("task").trim();
        if (StringUtils.isEmpty(configParam)) {
            taskVo = JSON.parseObject("{\"taskDetail\":{\"cronExpression\":\" */5 * * * * ?\",\"taskAliasName\":\"test\",\"isAutoRun\":\"1\",\"taskGroupName\":\"省分-sf\",\"taskDesc\":\"任务描述\"},\"excel\":{\"excelModelId\":\"\"},\"dataSource\":{\"dataSourceModelId\":\"\"},\"picture\":{\"pictureModelId\":\"\"},\"email\":{\" emailModelId\":\"\"},\"yixin\":{\"yixinModelId\":\"\"}}", TaskVo.class);
        } else {
            taskVo = JSON.parseObject(configParam, TaskVo.class);
        }
        rd = new ResultDto();
        try {
            rd = taskService.save(taskVo);
        } catch (Exception ex) {
            LOG.info("save task failue :" + ex.getMessage());
            rd.setInfo("save task failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ResultDto update(HttpServletRequest request) {
        TaskVo taskVo = null;
        ResultDto rd = null;
        String configParam = request.getParameter("dataSource").trim();
        if (StringUtils.isEmpty(configParam)) {
            taskVo = JSON.parseObject("{\"dataSourceURL\":\"jdbc:oracle:thin:@localhost:1521:ORCL\",\"userName\":\"demo\",\"password\":\"123\",\"sqlList\":[\"select * from pushtest\",\"select *  from pushtest\"],\"excelList\":[\"excelId1\",\"excelId2\"]}", TaskVo.class);
        } else {
            taskVo = JSON.parseObject(configParam, TaskVo.class);
        }
        rd = new ResultDto();
        try {
            rd = taskService.update(taskVo);
        } catch (Exception ex) {
            rd.setInfo("update  dataSource failue").setStatus(ResultInfo.failure);
        }
        return rd;
    }


}
