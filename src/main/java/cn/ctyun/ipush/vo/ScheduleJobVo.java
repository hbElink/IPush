package cn.ctyun.ipush.vo;

import com.dexcoder.commons.pager.Pageable;

import java.util.Date;

/**
 * Created by liyd on 12/19/14.
 */
public class ScheduleJobVo extends Pageable {

    private static final long  serialVersionUID = -4216107640768329946L;

    /** 任务调度的参数key */
    public static final String JOB_PARAM_TASKVO   = "taskVo";
    public static final String JOB_PARAM_CONNECTIONPARAMS    = "connectionParams";
    public static final String JOB_PARAM_SCHEDULEJOBVO    = "scheduleJobVo";
    public static final String   JOB_PARAM_KEY ="jobParam";

    /** 任务id */
    private String               scheduleJobId;

    /** 任务名称 */
    private String             jobName;

    /** 任务别名 */
    private String             aliasName;

    /** 任务分组 */
    private String             jobGroup;

    /** 触发器 */
    private String             jobTrigger;

    /** 任务状态 */
    private String             status;

    /** 任务运行时间表达式 */
    private String             cronExpression;

    /** 是否异步 */
    private Boolean            isSync;

    /** 任务描述 */
    private String             description;

    /** 创建时间 */
    private String               gmtCreate;

    /** 修改时间 */
    private String               gmtModify;

    /*****************************************/
    private String              isAutoRun; //任务是否是自启动

    private String                taskId;  //任务ID

    public Boolean getSync() {
        return isSync;
    }

    public void setSync(Boolean sync) {
        isSync = sync;
    }

    public String getIsAutoRun() {
        return isAutoRun;
    }

    public void setIsAutoRun(String isAutoRun) {
        this.isAutoRun = isAutoRun;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getScheduleJobId() {
        return scheduleJobId;
    }

    public void setScheduleJobId(String scheduleJobId) {
        this.scheduleJobId = scheduleJobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobTrigger() {
        return jobTrigger;
    }

    public void setJobTrigger(String jobTrigger) {
        this.jobTrigger = jobTrigger;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(String gmtModify) {
        this.gmtModify = gmtModify;
    }

    public Boolean getIsSync() {
        return isSync;
    }

    public void setIsSync(Boolean isSync) {
        this.isSync = isSync;
    }
}
