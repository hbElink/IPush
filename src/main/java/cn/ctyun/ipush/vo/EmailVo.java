package cn.ctyun.ipush.vo;

import cn.ctyun.ipush.model.EmailModel;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-12
 * Time: 下午8:20
 * To change this template use File | Settings | File Templates.
 */
public class EmailVo extends EmailModel {

    private List<String> emailReceivers;

    public List<String> getEmailReceivers() {
        return emailReceivers;
    }

    public void setEmailReceivers(List<String> emailReceivers) {
        this.emailReceivers = emailReceivers;
    }
}
