package cn.ctyun.ipush.vo;

import cn.ctyun.ipush.model.DataSourceModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-12
 * Time: 下午8:05
 * To change this template use File | Settings | File Templates.
 */
public class DataSourceVo extends DataSourceModel {

    private List<String> sqlListVo;
    private List<String> excelListVo;
    private Map<String,String> sqlExcelMapVo=new HashMap<String ,String>();

    public List<String> getSqlListVo() {
        return sqlListVo;
    }

    public void setSqlListVo(List<String> sqlListVo) {
        this.sqlListVo = sqlListVo;
    }

    public List<String> getExcelListVo() {
        return excelListVo;
    }

    public void setExcelListVo(List<String> excelListVo) {
        this.excelListVo = excelListVo;
    }

    public Map<String, String> getSqlExcelMapVo() {
        return sqlExcelMapVo;
    }

    public void setSqlExcelMapVo(Map<String, String> sqlExcelMapVo) {
        this.sqlExcelMapVo = sqlExcelMapVo;
    }
}
