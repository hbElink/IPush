package cn.ctyun.ipush.vo;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-14
 * Time: 上午9:24
 * To change this template use File | Settings | File Templates.
 */
public class ResultVo implements Serializable {
     private String status; //success  fail
     private String info;   //error or  normal result json

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
