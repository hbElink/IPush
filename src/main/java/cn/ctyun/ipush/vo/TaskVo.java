package cn.ctyun.ipush.vo;

import cn.ctyun.ipush.model.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-12
 * Time: 下午8:06
 * To change this template use File | Settings | File Templates.
 */
public class TaskVo extends TaskModel {

    private TaskDetailModel taskDetail;
    private DataSourceModel dataSource;
    private ExcelModel excel;
    private PictureModel picture;
    private EmailModel email;
    private  YixinModel yixin;

    public TaskDetailModel getTaskDetail() {
        return taskDetail;
    }

    public void setTaskDetail(TaskDetailModel taskDetail) {
        this.taskDetail = taskDetail;
    }

    public DataSourceModel getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSourceModel dataSource) {
        this.dataSource = dataSource;
    }

    public ExcelModel getExcel() {
        return excel;
    }

    public void setExcel(ExcelModel excel) {
        this.excel = excel;
    }

    public PictureModel getPicture() {
        return picture;
    }

    public void setPicture(PictureModel picture) {
        this.picture = picture;
    }

    public EmailModel getEmail() {
        return email;
    }

    public void setEmail(EmailModel email) {
        this.email = email;
    }

    public YixinModel getYixin() {
        return yixin;
    }

    public void setYixin(YixinModel yixin) {
        this.yixin = yixin;
    }
}
