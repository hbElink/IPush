package cn.ctyun.ipush.dao;

import cn.ctyun.ipush.model.*;
import cn.ctyun.ipush.utils.DateUtils;
import cn.ctyun.ipush.vo.TaskVo;
import com.alibaba.fastjson.JSON;
import com.dexcoder.commons.utils.UUIDUtils;
import com.dexcoder.dal.JdbcDao;
import com.dexcoder.dal.build.Criteria;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-16
 * Time: 上午11:22
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TaskDao {

    /* 日志对象 */
    private final Logger LOG = LoggerFactory.getLogger(TaskDao.class);

    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private TaskDetailDao taskDetailDao;

    /**
     * task 任务保存，其中，dataSource/excel/email/picture/yixin 都需要新建
     *
     * @param taskVo
     * @return
     */
    public boolean insert(TaskVo taskVo) {
        //add model primary key
        TaskModel tm = new TaskModel();
        tm.setTaskModelId(UUIDUtils.getUUID32());
        tm.setTaskDetailId(UUIDUtils.getUUID32());
        tm.setExcelId(UUIDUtils.getUUID32());
        tm.setDataSourceId(UUIDUtils.getUUID32());
        tm.setEmailId(UUIDUtils.getUUID32());
        tm.setPictureId(UUIDUtils.getUUID32());
        tm.setYixinId(UUIDUtils.getUUID32());

        TaskDetailModel taskDetailModel = taskVo.getTaskDetail();
        taskDetailModel.setTaskDetailModelId(tm.getTaskDetailId());
        taskDetailModel.setTaskGroupName(taskDetailModel.getTaskGroupName());
        taskDetailModel.setCronExpression(taskDetailModel.getCronExpression());
        taskDetailModel.setTaskName(taskDetailModel.getTaskGroupName() + "_" + "task_" + DateUtils.timestampToDataString(System.currentTimeMillis()));
        taskDetailModel.setCreateDate(DateUtils.dateToString(new Date()));
        taskDetailModel.setIsAutoRun("1");
        taskDetailModel.setIsDeleted("0");

        DataSourceModel dataSourceModel = taskVo.getDataSource();
        dataSourceModel.setDataSourceModelId(tm.getDataSourceId());
        dataSourceModel.setConnMaxCount("5");//现在无意义 现在是每一个时间点是一个连接
        dataSourceModel.setCreateDate(DateUtils.dateToString(new Date()));
        dataSourceModel.setIsDeleted("0");


        taskVo.getTaskDetail().setTaskDetailModelId(tm.getTaskDetailId());
        taskVo.getDataSource().setDataSourceModelId(tm.getDataSourceId());
        taskVo.getExcel().setExcelModelId(tm.getExcelId());
        taskVo.getPicture().setPictureModelId(tm.getPictureId());
        taskVo.getEmail().setEmailModelId(tm.getEmailId());
        taskVo.getYixin().setYixinModelId(tm.getYixinId());

        //系统定的字段值
        // taskVo.getTaskDetail().setTaskName();

        try {
            jdbcDao.save(tm);
            jdbcDao.save(dataSourceModel);
            jdbcDao.save(taskDetailModel);
            jdbcDao.save(taskVo.getExcel());
            jdbcDao.save(taskVo.getPicture());
            jdbcDao.save(taskVo.getEmail());
            jdbcDao.save(taskVo.getYixin());
            LOG.info("task : " + JSON.toJSONString(tm) + " save success");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * task 任务保存，其中，dataSource/excel/email/picture/yixin 主键id都存在
     *
     * @param taskVo
     */
    public boolean insert(TaskVo taskVo, boolean isExistParamsId) {
        //add model primary key
        TaskModel tm = taskVo.getTargetObject(TaskModel.class);
        tm.setTaskModelId(UUIDUtils.getUUID32());
        tm.setTaskDetailId(UUIDUtils.getUUID32());

        TaskDetailModel taskDetailModel = taskVo.getTaskDetail();
        taskDetailModel.setTaskDetailModelId(tm.getTaskDetailId());
        taskDetailModel.setTaskGroupName(taskDetailModel.getTaskGroupName());
        taskDetailModel.setTaskName("task_" + DateUtils.timestampToDataString(System.currentTimeMillis()));
        taskDetailModel.setCreateDate(DateUtils.dateToString(new Date()));
        taskDetailModel.setCronExpression(taskDetailModel.getCronExpression());
        taskDetailModel.setIsAutoRun("1");
        taskDetailModel.setIsDeleted("0");

        try {
            jdbcDao.save(tm);
            taskDetailDao.save(taskDetailModel);
            LOG.info("task : " + JSON.toJSONString(tm) + " save success");
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * 更新任务task
     *
     * @param taskModel
     * @return
     */
    public int update(TaskModel taskModel) {
        if (taskModel.getTaskDetailId() == null) {
            LOG.info("Error taskModel id 更新不能为空");
            return 0;
        }
        taskModel.setModifyDate(DateUtils.dateToString(new Date()));
        return jdbcDao.update(taskModel);
    }

    /**
     * @param taskModelId
     * @return
     */
    public int delete(String taskModelId) {
        if (StringUtils.isEmpty(taskModelId)) {
            LOG.info("Error taskModel id 更新不能为空");
            return 0;
        }
        Criteria criteria=Criteria.update(TaskModel.class).set("isDeleted","1").where("taskModelId",new Object[]{taskModelId});
        return jdbcDao.update(criteria);
    }

    public TaskModel querySingleResult(TaskModel taskModel) {
        return jdbcDao.querySingleResult(taskModel);
    }

    public List<TaskModel> queryList() {
        return jdbcDao.queryList(Criteria.select(TaskModel.class));
    }

    public List<TaskModel> queryList(Criteria criteria) {
        return jdbcDao.queryList(criteria);
    }

    /**
     * @param taskModel 完整的taskModel 数据库对象
     * @return
     */
    public TaskVo getTaskVoByTaskModel(TaskModel taskModel) {

        TaskVo taskVo = new TaskVo();

        taskVo.setTaskModelId(taskModel.getTaskModelId());

        DataSourceModel dataSourceModel = new DataSourceModel();
        dataSourceModel.setDataSourceModelId(taskModel.getDataSourceId());

        TaskDetailModel taskDetailModel = new TaskDetailModel();
        taskDetailModel.setTaskDetailModelId(taskModel.getTaskDetailId());

        ExcelModel excelModel = new ExcelModel();
        excelModel.setExcelModelId(taskModel.getExcelId());

        PictureModel pictureModel = new PictureModel();
        pictureModel.setPictureModelId(taskModel.getPictureId());

        EmailModel emailModel = new EmailModel();
        emailModel.setEmailModelId(taskModel.getEmailId());

        YixinModel yixinModel = new YixinModel();
        yixinModel.setYixinModelId(taskModel.getYixinId());

        try {

            dataSourceModel = jdbcDao.querySingleResult(dataSourceModel);
            taskDetailModel = jdbcDao.querySingleResult(taskDetailModel);
            excelModel = jdbcDao.querySingleResult(excelModel);
            pictureModel = jdbcDao.querySingleResult(pictureModel);
            emailModel = jdbcDao.querySingleResult(emailModel);
            yixinModel = jdbcDao.querySingleResult(yixinModel);

            taskVo.setDataSource(dataSourceModel);
            taskVo.setTaskDetail(taskDetailModel);
            taskVo.setExcel(excelModel);
            taskVo.setPicture(pictureModel);
            taskVo.setEmail(emailModel);
            taskVo.setYixin(yixinModel);
            return taskVo;

        } catch (Exception ex) {
            LOG.info("taskModel convert taskVo fail");
            return null;
        }

    }

    /**
     * @param taskVo 前端转换的task视图
     * @return 返回完整的taskVo对象
     */
    public TaskVo getNewTaskVoFromTaskVo(TaskVo taskVo) {

        if (taskVo.getTaskModelId() == null || taskVo.getTaskDetailId() == null || taskVo.getDataSourceId() == null || taskVo.getYixinId() == null || taskVo.getEmailId() == null || taskVo.getPictureId() == null || taskVo.getExcelId() == null)
            LOG.info("taskVo 不完整 ");

        DataSourceModel dataSourceModel = new DataSourceModel();
        dataSourceModel.setDataSourceModelId(taskVo.getDataSourceId());
        dataSourceModel = jdbcDao.querySingleResult(dataSourceModel);
        taskVo.setDataSource(dataSourceModel);

        ExcelModel excelModel = new ExcelModel();
        excelModel.setExcelModelId(taskVo.getExcelId());
        excelModel = jdbcDao.querySingleResult(excelModel);
        taskVo.setExcel(excelModel);

        EmailModel emailModel = new EmailModel();
        emailModel.setEmailModelId(taskVo.getEmailId());
        emailModel = jdbcDao.querySingleResult(emailModel);
        taskVo.setEmail(emailModel);

        PictureModel pictureModel = new PictureModel();
        pictureModel.setPictureModelId(taskVo.getPictureId());
        pictureModel = jdbcDao.querySingleResult(pictureModel);
        taskVo.setPicture(pictureModel);

        YixinModel yixinModel = new YixinModel();
        yixinModel.setYixinModelId(taskVo.getYixinId());
        yixinModel = jdbcDao.querySingleResult(yixinModel);
        taskVo.setYixin(yixinModel);

        return taskVo;


    }

    /**
     * 根据taskID 得到TaskVo
     *
     * @param taskId
     * @return
     */
    public TaskVo getDbTaskVoByTaskId(String taskId) {
        Criteria criteria = Criteria.select(TaskModel.class).where("taskModelId", new Object[]{taskId});
        TaskModel taskModel = jdbcDao.querySingleResult(criteria);
        return getTaskVoByTaskModel(taskModel);
    }

    /**
     * 数据源是否在task中存在，判断数据源删除的条件
     * @param dataSourceId
     * @return
     */
    public boolean isExistDataSourceInTask(String dataSourceId) {
        Criteria criteria=Criteria.select(TaskModel.class).where("dataSourceModelId",new Object[]{dataSourceId});
        int count=jdbcDao.queryCount(criteria);
        if(count<1){
            return false;
        }
        return true;
    }
}
