package cn.ctyun.ipush.dao;

import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.utils.DateUtils;
import com.dexcoder.dal.JdbcDao;
import com.dexcoder.dal.build.Criteria;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-18
 * Time: 下午12:56
 * To change this template use File | Settings | File Templates.
 */
@Component
public class DataSourceDao {

    @Autowired
    private JdbcDao jdbcDao;

    /**
     * 保存
     *
     * @param dataSourceModel
     */
    public void save(DataSourceModel dataSourceModel) {
        jdbcDao.save(dataSourceModel);
    }

    /**
     * 更新 ,主键Id 必须存在;此数据是修改后的完整对象
     *
     * @param dataSourceModel
     */
    public int update(DataSourceModel dataSourceModel) {
        if (StringUtils.isEmpty(dataSourceModel.getDataSourceModelId()))
            throw new RuntimeException("dataSourceModel 主键id 必须设置");
        dataSourceModel.setModifyDate(DateUtils.dateToString(new Date()));
        return jdbcDao.update(dataSourceModel);
    }


    /**
     * 更新 主键Id 必须存在
     *
     * @param dataSourceModelId
     */
    public int delete(String dataSourceModelId) {
        if (StringUtils.isEmpty(dataSourceModelId))
            throw new RuntimeException("dataSourceModel 主键id 必须设置");
        Criteria criteria = Criteria.update(DataSourceModel.class).where("dataSourceModelId", new Object[]{dataSourceModelId}).set("isDeleted", "1");
        return jdbcDao.update(criteria);
    }

    /**
     * 查询单个
     *
     * @param dataSourceModelId
     * @return
     */
    public DataSourceModel querySingleResult(String dataSourceModelId) {
        if (StringUtils.isEmpty(dataSourceModelId))
            throw new RuntimeException("dataSourceModel 主键id 必须设置");
        Criteria criteria = Criteria.select(DataSourceModel.class).where("dataSourceModelId", new Object[]{dataSourceModelId});
        return jdbcDao.querySingleResult(criteria);
    }

    /**
     * 查询多个
     *
     * @param dataSourceModel
     * @return
     */
    public List<DataSourceModel> queryList(DataSourceModel dataSourceModel) {
        if (null == dataSourceModel)
            throw new RuntimeException("dataSourceModel 为空");
        return jdbcDao.queryList(dataSourceModel);
    }

}
