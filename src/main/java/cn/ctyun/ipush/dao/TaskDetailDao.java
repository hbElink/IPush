package cn.ctyun.ipush.dao;

import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.model.TaskModel;
import cn.ctyun.ipush.utils.DateUtils;
import com.dexcoder.dal.JdbcDao;
import com.dexcoder.dal.build.Criteria;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-17
 * Time: 下午9:19
 * To change this template use File | Settings | File Templates.
 */
@Component
public class TaskDetailDao {

    /* 日志对象 */
    private final Logger LOG = LoggerFactory.getLogger(TaskDetailDao.class);


    @Autowired
    private JdbcDao jdbcDao;

    public void   save(TaskDetailModel taskDetailModel){
        if (null==taskDetailModel) {
            LOG.info("Error taskDetailModel 不能为空");
        }
        jdbcDao.save(taskDetailModel);
    }

    /**
     * @param taskDetailId
     * @return
     */
    public int delele(String taskDetailId) {
        if (StringUtils.isEmpty(taskDetailId)) {
            LOG.info("Error taskDetail id 更新不能为空");
            return 0;
        }
        Criteria criteria=Criteria.update(TaskDetailModel.class).set("isDeleted","1").where("taskDetailModelId",new Object[]{taskDetailId});
        return jdbcDao.update(criteria);

    }

    /**
     *
     * @param taskDetailModel
     * @return
     */
    public int update(TaskDetailModel taskDetailModel){
        if(taskDetailModel.getTaskDetailModelId()==null)
            LOG.info("Error taskDetailModel 更新 id 不能为空 ");
        taskDetailModel.setModifyDate(DateUtils.dateToString(new Date()));
        return jdbcDao.update(taskDetailModel);
    }

    /**
     * @param taskDetailModel
     * @return
     */
    public TaskDetailModel querySingleModel(TaskDetailModel taskDetailModel){
        return jdbcDao.querySingleResult(taskDetailModel);
    }

    public TaskDetailModel querySingleModel(String  taskDetailModelId){
        TaskDetailModel taskDetailModel=new TaskDetailModel();
        taskDetailModel.setTaskDetailModelId(taskDetailModelId);
        return jdbcDao.querySingleResult(taskDetailModel);
    }

    /**
     * taskGroupname+taskName unique Task
     *
     * @param taskDetailModel
     * @return
     */
    public boolean isExist(TaskDetailModel taskDetailModel) {
        TaskDetailModel tdm = new TaskDetailModel();
        tdm.setTaskGroupName(taskDetailModel.getTaskGroupName());
        tdm.setTaskAliasName(taskDetailModel.getTaskAliasName());
        int count = jdbcDao.queryCount(tdm);
        if (count < 1) {
            return false;
        } else {
            return true;
        }
    }




}
