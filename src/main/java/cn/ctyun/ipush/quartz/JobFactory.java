package cn.ctyun.ipush.quartz;

import cn.ctyun.ipush.dto.ConnectionObject;
import cn.ctyun.ipush.event.ToolSpring;
import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.service.TaskService;
import cn.ctyun.ipush.utils.DBUtils;
import cn.ctyun.ipush.utils.DataUtils;
import cn.ctyun.ipush.utils.DateUtils;
import cn.ctyun.ipush.utils.ScheduleUtils;
import cn.ctyun.ipush.vo.ScheduleJobVo;
import cn.ctyun.ipush.vo.TaskVo;
import com.alibaba.fastjson.JSON;
import oracle.jdbc.OraclePreparedStatement;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * 任务工厂类,非同步
 * <p/>
 * User: liyd
 * Date: 14-1-3
 * Time: 上午10:11
 */
@DisallowConcurrentExecution //保证频率高时，同步执行
@PersistJobDataAfterExecution //jobDataMap数据更新支持
public class JobFactory implements Job {

    /* 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(JobFactory.class);

    public void execute(JobExecutionContext context) throws JobExecutionException {

        Long startTime = System.currentTimeMillis();

        ScheduleJobVo scheduleJobVo = (ScheduleJobVo) context.getMergedJobDataMap().get(ScheduleJobVo.JOB_PARAM_SCHEDULEJOBVO);
        Map<String, String> connectionDescMap = (Map<String, String>) context.getMergedJobDataMap().get(ScheduleJobVo.JOB_PARAM_CONNECTIONPARAMS);

        TaskService taskService = (TaskService) ToolSpring.getApplicationContext().getBean("taskService");
        TaskVo taskVo = taskService.getTaskVoByTaskId(scheduleJobVo.getTaskId());//(TaskVo)context.getMergedJobDataMap().get(ScheduleJobVo.JOB_PARAM_TASKVO); //
        String currentTrigger = context.getTrigger().getKey().getName();

        TaskDetailModel taskDetailModel = taskVo.getTaskDetail();
        //数据源被删除
        if (taskDetailModel == null) {
            LOG.info(JSON.toJSONString(connectionDescMap) + ",数据源不存在");
            return;
        }
        List<String> newConnectionDescList = new ArrayList();
        newConnectionDescList.add(0, taskVo.getDataSource().getDataSourceUrl());
        newConnectionDescList.add(1, taskVo.getDataSource().getUserName());
        newConnectionDescList.add(2, taskVo.getDataSource().getPassword());
        newConnectionDescList.add(3, taskVo.getTaskDetail().getTaskGroupName());
        newConnectionDescList.add(4, taskVo.getTaskDetail().getTaskName());
        newConnectionDescList.add(5, taskVo.getTaskDetail().getCronExpression());

        Map<String, String> newConnectionDescMap = new HashMap<String, String>();
        connectionDescMap.put("url", taskVo.getDataSource().getDataSourceUrl());
        connectionDescMap.put("username", taskVo.getDataSource().getUserName());
        connectionDescMap.put("password", taskVo.getDataSource().getPassword());
        connectionDescMap.put("groupname", taskVo.getTaskDetail().getTaskGroupName());
        connectionDescMap.put("jobname", taskVo.getTaskDetail().getTaskName());
        connectionDescMap.put("cron", taskVo.getTaskDetail().getCronExpression());

        LOG.info(String.format("--- JobTask start execute : jobGroup: %s ,jobName: %s,TriggerTime: %s ,execute Time: %s---------- ", scheduleJobVo.getJobGroup(), scheduleJobVo.getJobName(), currentTrigger, DateUtils.timestampToDataString(System.currentTimeMillis())));
        Connection conn = null;
        OraclePreparedStatement pstmt = null;
        ResultSet rs = null;
        String userName = taskVo.getDataSource().getUserName();
        String password = taskVo.getDataSource().getPassword();
        String url = taskVo.getDataSource().getDataSourceUrl();
        String jobGroup = scheduleJobVo.getJobGroup();
        String jobName = scheduleJobVo.getJobName();
        //先判斷一下緩存是否更新，如果更新 先刪除，在創建，在使用
        context.getJobDetail().getJobDataMap().put("ScheduleJobVo.JOB_PARAM_CONNECTIONPARAMS", newConnectionDescMap);
        conn = connectionCacheUpdate(connectionDescMap, newConnectionDescList, currentTrigger);
        boolean isTmpConnection = false;
        try {
            if (null == conn || conn.isClosed()) {
                isTmpConnection = true;
                conn = DBUtils.getTmpConnection(url, userName, password, jobGroup, jobName, currentTrigger);
            }
            //conn = DBUtils.getOracleConnection(url, userName, password, jobGroup, jobName, currentTrigger);  //ConnectionPoolService.getCurrentConnecton(taskVo.getDataSource());
            if (null == conn) {
                LOG.info("Error:  task time span is too short");
                return;
            }
            Map<String, Object> rsMap = DBUtils.executeOracleSelect(conn, "select *  from pushtest");
            rs = (ResultSet) rsMap.get("rs");
            pstmt = (OraclePreparedStatement) rsMap.get("ps");
            if (null != rs) {
                int col = 0;
                col = rs.getMetaData().getColumnCount();
                for (int j = 1; j < col + 1; j++) {
                    System.out.print(rs.getMetaData().getColumnName(j) + "\t");
                }
                System.out.println();
                while (rs.next()) {
                    for (int j = 1; j < col + 1; j++) {
                        System.out.print(rs.getString(j) + "\t");
                    }
                    System.out.println();
                }
            } else {
                LOG.info("--- JobTask execute end : resultSet is null----------");
            }
            pstmt.close();
            long endTime = System.currentTimeMillis();
            LOG.info(String.format("--- JobTask execute emd , cost time: %s msec----------", endTime - startTime));
        } catch (SQLException e) {
            LOG.info(e.getMessage());
        } catch (Exception ex) {
            LOG.info(ex.getMessage());
        } finally {
            try {
                if (null != pstmt)
                    pstmt.close();
            } catch (SQLException e) {
                LOG.info("ERROR SQL statment execute fail");
            }
            if (isTmpConnection) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    LOG.info("---ERROR: tmp connection fail");
                }
            } else {
                DBUtils.releaseConn(url, userName, password, jobGroup, jobName, currentTrigger);
                LOG.info("---realease connection success");
            }
        }

        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 看当前时间点连接，是否需要更新连接池缓存
     *
     * @param connectionDescMap     当前存在的
     * @param newConnectionDescList 从数据库得到的最新的
     */
    private Connection connectionCacheUpdate(Map<String, String> connectionDescMap, List<String> newConnectionDescList, String currentTrigger) {
        //判断是否相等 是否可用
        String connectionIdentity = DBUtils.getConnnectionIdentity(connectionDescMap.get("url"), connectionDescMap.get("username"), connectionDescMap.get("password"), connectionDescMap.get("groupname"), currentTrigger);
        List<String> oldConnectionDescList = new ArrayList();
        oldConnectionDescList.add(0, connectionDescMap.get("url"));
        oldConnectionDescList.add(1, connectionDescMap.get("username"));
        oldConnectionDescList.add(2, connectionDescMap.get("password"));
        oldConnectionDescList.add(3, connectionDescMap.get("groupname"));
        oldConnectionDescList.add(4, connectionDescMap.get("jobname"));
        oldConnectionDescList.add(5, connectionDescMap.get("cron"));
        if (DataUtils.compare(oldConnectionDescList, newConnectionDescList)) { //缓存存在，判断是否有效、可用 更新dataSource
            ConnectionObject connectionObject = DBUtils.connectionCacheMap.get(connectionIdentity);
            try {
                if (connectionObject.getConnection() != null && !connectionObject.getConnection().isClosed() && !connectionObject.getIsUsed()) {
                    return connectionObject.getConnection();
                } else {
                    DBUtils.deleteConnectionCache(connectionIdentity);
                    return DBUtils.getOracleConnection(oldConnectionDescList.get(0), oldConnectionDescList.get(1), oldConnectionDescList.get(2), oldConnectionDescList.get(3), oldConnectionDescList.get(4), currentTrigger);
                }
            } catch (SQLException e) {
                LOG.info("得到缓存连接失败");
                return null;
            }
        } else { //缓存不存在  将新的连接放在jobDataMap中
            //当前旧的连接是否在用
            ConnectionObject oldConnectionObject = DBUtils.connectionCacheMap.get(connectionIdentity);
            if (oldConnectionObject.getIsUsed()) {
                LOG.info("---作业执行间隔太短，连接在被占用");
                return null;
            } else {
                //删除旧连接
                DBUtils.deleteConnectionCache(connectionIdentity);
                //创建新的连接
                try {
                    for (String cron : newConnectionDescList.get(5).split("@")) {
                        if (ScheduleUtils.trigerNameFormat(newConnectionDescList.get(4), cron).equals(currentTrigger))
                            return DBUtils.getOracleConnection(newConnectionDescList.get(0), newConnectionDescList.get(1), newConnectionDescList.get(2), newConnectionDescList.get(3), newConnectionDescList.get(4), cron);
                    }
                } catch (SQLException e) {
                    LOG.info("创建新连接失败");
                    return null;
                }
            }
        }
        return null;
    }
}
