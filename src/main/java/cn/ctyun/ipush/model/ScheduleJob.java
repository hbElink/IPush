package cn.ctyun.ipush.model;

import com.dexcoder.commons.pager.Pageable;

import java.util.Date;

/**
 * 计划任务信息
 * 
 * User: liyd
 * Date: 14-1-3
 * Time: 上午10:24
 */
public class ScheduleJob extends Pageable {

    private static final long serialVersionUID = 4888005949821878223L;

    /** 任务id */
    private String              scheduleJobId;

    /** 任务名称 */
    private String            jobName;

    /** 任务别名 */
    private String            aliasName;

    /** 任务分组 */
    private String            jobGroup;

    /** 触发器 */
    private String            jobTrigger;

    /** 任务状态 */
    private String            status;

    /** 任务运行时间表达式 */
    private String            cronExpression;

    /** 是否异步 */
    private String           isSync;

    /** 任务描述 */
    private String            description;

    /** 创建时间 */
    private String              gmtCreate;

    /** 修改时间 */
    private String              gmtModify;



    /*****************************************/
    private String              isAutoRun; //任务是否是自启动

    private String                taskId;  //任务ID

    private String             isDeleted;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getIsSync() {
        return isSync;
    }

    public void setIsSync(String isSync) {
        this.isSync = isSync;
    }

    public String getIsAutoRun() {
        return isAutoRun;
    }

    public void setIsAutoRun(String isAutoRun) {
        this.isAutoRun = isAutoRun;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    /**
     * 任务配置参数
     * @return
     */
//    private String  dataSource;
//
//    private String  excelSource;
//
//    private String  excelDataName;
//
//    private String  pictureName;
//
//    private String  yiXinAppid;

//    @Override
//    public String  toString(){
//        return String.format("aliasname:s%,description:s%",this.aliasName,this.description);
//    }

//    public Boolean getSync() {
//        return isSync;
//    }
//
//    public void setSync(Boolean sync) {
//        isSync = sync;
//    }

//    public String getDataSource() {
//        return dataSource;
//    }
//
//    public void setDataSource(String dataSource) {
//        this.dataSource = dataSource;
//    }
//
//    public String getExcelSource() {
//        return excelSource;
//    }
//
//    public void setExcelSource(String excelSource) {
//        this.excelSource = excelSource;
//    }
//
//    public String getExcelDataName() {
//        return excelDataName;
//    }
//
//    public void setExcelDataName(String excelDataName) {
//        this.excelDataName = excelDataName;
//    }
//
//    public String getPictureName() {
//        return pictureName;
//    }
//
//    public void setPictureName(String pictureName) {
//        this.pictureName = pictureName;
//    }
//
//    public String getYiXinAppid() {
//        return yiXinAppid;
//    }
//
//    public void setYiXinAppid(String yiXinAppid) {
//        this.yiXinAppid = yiXinAppid;
//    }

    public String getScheduleJobId() {
        return scheduleJobId;
    }

    public void setScheduleJobId(String scheduleJobId) {
        this.scheduleJobId = scheduleJobId;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getJobGroup() {
        return jobGroup;
    }

    public void setJobGroup(String jobGroup) {
        this.jobGroup = jobGroup;
    }

    public String getJobTrigger() {
        return jobTrigger;
    }

    public void setJobTrigger(String jobTrigger) {
        this.jobTrigger = jobTrigger;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(String gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getGmtModify() {
        return gmtModify;
    }

    public void setGmtModify(String gmtModify) {
        this.gmtModify = gmtModify;
    }
}
