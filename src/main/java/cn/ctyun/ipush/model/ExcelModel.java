package cn.ctyun.ipush.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-11
 * Time: 下午3:43
 * To change this template use File | Settings | File Templates.
 */
public class ExcelModel implements Serializable {

    private String excelModelId;
    private String excelName;
    private String createDate;


    private String templateId;
    private String templateName;
    private String templateCreateDate;
    private String templateBelongUserid;

    private String isDeleted;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }
    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getTemplateId() {
        return templateId;
    }

    public void setTemplateId(String templateId) {
        this.templateId = templateId;
    }

    public String getExcelModelId() {
        return excelModelId;
    }

    public void setExcelModelId(String excelModelId) {
        this.excelModelId = excelModelId;
    }

    public String getExcelName() {
        return excelName;
    }

    public void setExcelName(String excelName) {
        this.excelName = excelName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateCreateDate() {
        return templateCreateDate;
    }

    public void setTemplateCreateDate(String templateCreateDate) {
        this.templateCreateDate = templateCreateDate;
    }

    public String getTemplateBelongUserid() {
        return templateBelongUserid;
    }

    public void setTemplateBelongUserid(String templateBelongUserid) {
        this.templateBelongUserid = templateBelongUserid;
    }
}
