package cn.ctyun.ipush.model;

import com.dexcoder.commons.pager.Pageable;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: wjf
 * Date: 16-6-11
 * Time: 下午1:32
 * To change this template use File | Settings | File Templates.
 */
public class DataSourceModel extends Pageable implements Serializable {

    private String dataSourceModelId;
    private String dataSourceUrl;
    private String userName;
    private String password;
    private String sqlListDb;
    private String excelListDb;
    private String createDate;
    private String createUserid;
    private String dataSourceDriver;

    private String connMaxCount;
    private String isDeleted;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getDataSourceDriver() {
        return dataSourceDriver;
    }

    public void setDataSourceDriver(String dataSourceDriver) {
        this.dataSourceDriver = dataSourceDriver;
    }

    public String getConnMaxCount() {
        return connMaxCount;
    }

    public void setConnMaxCount(String connMaxCount) {
        this.connMaxCount = connMaxCount;
    }

    public String getSqlListDb() {
        return sqlListDb;
    }

    public void setSqlListDb(String sqlListDb) {
        this.sqlListDb = sqlListDb;
    }

    public String getExcelListDb() {
        return excelListDb;
    }

    public void setExcelListDb(String excelListDb) {
        this.excelListDb = excelListDb;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserid() {
        return createUserid;
    }

    public void setCreateUserid(String createUserid) {
        this.createUserid = createUserid;
    }

    public String getDataSourceModelId() {
        return dataSourceModelId;
    }

    public void setDataSourceModelId(String dataSourceModelId) {
        this.dataSourceModelId = dataSourceModelId;
    }

    public String getDataSourceUrl() {
        return dataSourceUrl;
    }

    public void setDataSourceUrl(String dataSourceUrl) {
        this.dataSourceUrl = dataSourceUrl;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
