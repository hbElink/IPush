package cn.ctyun.ipush.model;

import com.dexcoder.commons.pager.Pageable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-11
 * Time: 下午6:47
 * To change this template use File | Settings | File Templates.
 */
public class TaskDetailModel extends Pageable implements Serializable  {

    private String taskDetailModelId;
    private  String cronExpression;
    private  String taskName;
    private  String taskGroupName;
    private  String taskDesc;

    private  String  isAutoRun;

    private String createDate;
    private String createUserid;

    private String isDeleted;

    private String taskAliasName;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getTaskAliasName() {
        return taskAliasName;
    }

    public void setTaskAliasName(String taskAliasName) {
        this.taskAliasName = taskAliasName;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserid(){
        return createUserid;
    }

    public void setCreateUserid(String createUserid) {
        this.createUserid = createUserid;
    }

    public String getIsAutoRun() {
        return isAutoRun;
    }

    public void setIsAutoRun(String isAutoRun) {
        this.isAutoRun =isAutoRun;
    }

    public String getTaskDetailModelId() {
        return taskDetailModelId;
    }

    public void setTaskDetailModelId(String taskDetailModelId) {
        this.taskDetailModelId = taskDetailModelId;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskGroupName() {
        return taskGroupName;
    }

    public void setTaskGroupName(String taskGroupName) {
        this.taskGroupName = taskGroupName;
    }

    public String getTaskDesc() {
        return taskDesc;
    }

    public void setTaskDesc(String taskDesc) {
        this.taskDesc = taskDesc;
    }

}
