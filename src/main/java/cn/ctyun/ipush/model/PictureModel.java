package cn.ctyun.ipush.model;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-11
 * Time: 下午3:56
 * To change this template use File | Settings | File Templates.
 */
public class PictureModel implements Serializable {

    private String pictureModelId;
    private String picturePix;
    private String pictureFormat;
    private String isAddWatermark;
    private String createDate;
    private String createUserid;

    private String isDeleted;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }
    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }
    public String getPictureModelId() {
        return pictureModelId;
    }

    public void setPictureModelId(String pictureModelId) {
        this.pictureModelId = pictureModelId;
    }


    public String getPicturePix() {
        return picturePix;
    }

    public void setPicturePix(String picturePix) {
        this.picturePix = picturePix;
    }

    public String getPictureFormat() {
        return pictureFormat;
    }

    public void setPictureFormat(String pictureFormat) {
        this.pictureFormat = pictureFormat;
    }

    public String getIsAddWatermark() {
        return isAddWatermark;
    }

    public void setIsAddWatermark(String isAddWatermark) {
        this.isAddWatermark = isAddWatermark;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserid() {
        return createUserid;
    }

    public void setCreateUserid(String createUserid) {
        this.createUserid = createUserid;
    }
}
