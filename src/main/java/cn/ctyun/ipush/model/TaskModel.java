package cn.ctyun.ipush.model;

import com.dexcoder.commons.pager.Pageable;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: wjf
 * Date: 16-6-11
 * Time: 下午1:07
 * To change this template use File | Settings | File Templates.
 */
public class TaskModel extends Pageable implements Serializable {

    private  String taskModelId;

    private String taskDetailId;


    private String dataSourceId;

    private String excelId;


    private String pictureId;


    private String emailId;

    private String yixinId;

    private String createDate;
    private String createUserid;

    private String isDeleted;
    private String modifyDate;

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getCreateUserid() {
        return createUserid;
    }

    public void setCreateUserid(String createUserid) {
        this.createUserid = createUserid;
    }

    public String getTaskModelId() {
        return taskModelId;
    }

    public void setTaskModelId(String taskModelId) {
        this.taskModelId = taskModelId;
    }

    public String getTaskDetailId() {
        return taskDetailId;
    }

    public void setTaskDetailId(String taskDetailId) {
        this.taskDetailId = taskDetailId;
    }

    public String getDataSourceId() {
        return dataSourceId;
    }

    public void setDataSourceId(String dataSourceId) {
        this.dataSourceId = dataSourceId;
    }

    public String getExcelId() {
        return excelId;
    }

    public void setExcelId(String excelId) {
        this.excelId = excelId;
    }

    public String getPictureId() {
        return pictureId;
    }

    public void setPictureId(String pictureId) {
        this.pictureId = pictureId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getYixinId() {
        return yixinId;
    }

    public void setYixinId(String yixinId) {
        this.yixinId = yixinId;
    }






}
