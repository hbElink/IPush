package cn.ctyun.ipush.dto;

import java.sql.Connection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-15
 * Time: 下午12:07
 * To change this template use File | Settings | File Templates.
 */
public class ConnectionObject {

    private boolean isUsed;
    private Connection connection;

    public boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean isUsed) {
        isUsed = isUsed;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    //    private String  connectStr;
//    private Map<Connection,String> connMap=new HashMap<Connection,String>();
//    private int freeConnCount;
//    private int activeConnCount;


//    public String getConnectStr() {
//        return connectStr;
//    }
//
//    public void setConnectStr(String connectStr) {
//        this.connectStr = connectStr;
//    }
//
//    public Map<Connection, String> getConnMap() {
//        return connMap;
//    }
//
//    public void setConnMap(Map<Connection, String> connMap) {
//        this.connMap = connMap;
//    }
//
//    public int getFreeConnCount() {
//        return freeConnCount;
//    }
//
//    public void setFreeConnCount(int freeConnCount) {
//        this.freeConnCount = freeConnCount;
//    }
//
//    public int getActiveConnCount() {
//        return activeConnCount;
//    }
//
//    public void setActiveConnCount(int activeConnCount) {
//        this.activeConnCount = activeConnCount;
//    }
}
