package cn.ctyun.ipush.dto;

import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.model.TaskModel;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-13
 * Time: 上午11:28
 * To change this template use File | Settings | File Templates.
 */
public class TaskDsDto  extends TaskModel {

    private String dataSourceUrl;

    public String getDataSourceUrl() {
        return dataSourceUrl;
    }

    public void setDataSourceUrl(String dataSourceUrl) {
        this.dataSourceUrl = dataSourceUrl;
    }
}
