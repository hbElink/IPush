package cn.ctyun.ipush.dto;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-14
 * Time: 上午9:33
 * To change this template use File | Settings | File Templates.
 */
public class ResultDto {
    private String status; //success  fail
    private Object info;   //error or  normal result json

    public String getStatus() {
        return status;
    }

    public ResultDto setStatus(String status) {
        this.status = status;
        return this;
    }

    public Object getInfo() {
        return info;
    }

    public ResultDto setInfo(Object info) {
        this.info = info;
        return this;
    }
}
