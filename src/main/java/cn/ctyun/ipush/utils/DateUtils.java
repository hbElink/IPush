package cn.ctyun.ipush.utils;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-14
 * Time: 下午2:50
 * To change this template use File | Settings | File Templates.
 */
public class DateUtils {

    /**
     * 将java.util.Date 格式转换为字符串格式'yyyy-MM-dd HH:mm:ss a'(12小时制)<br>
     * 如Sat May 11 17:23:22 CST 2002 to '2002-05-11 05:23:22 下午'<br>
     * @param time Date 日期<br>
     * @return String 字符串<br>
     */
    public static String dateToString(Date time){
        SimpleDateFormat formatter;
        formatter = new SimpleDateFormat ("yyyy-MM-dd HH:mm:ss ");
        String ctime = formatter.format(time);
        return ctime;
    }

    /**
     * 将时间戳（精确到毫秒）转换成日期格式字符串
     * @param time
     * @return
     */
    public static String timestampToDataString(long time){
        Timestamp ts = new Timestamp(time);
        String tsStr = "";
        DateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
        return sdf.format(ts);
    }

}
