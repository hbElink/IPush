package cn.ctyun.ipush.utils;

import cn.ctyun.ipush.dto.ConnectionObject;
import cn.ctyun.ipush.model.DataSourceModel;
import cn.ctyun.ipush.model.TaskDetailModel;
import cn.ctyun.ipush.vo.TaskVo;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import oracle.jdbc.OraclePreparedStatement;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.attribute.standard.JobName;
import java.sql.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-7
 * Time: 下午4:40
 * To change this template use File | Settings | File Templates.
 */
public class DBUtils {

    /* 日志对象 */
    private static final Logger LOG = LoggerFactory.getLogger(DBUtils.class);


    /* 缓存连接 */
    public static final Map<String, ConnectionObject> connectionCacheMap = new HashMap<String, ConnectionObject>();

//    /**
//     * mysql 获取连接
//     *
//     * @param connectionDesc 连接串 eg: "jdbc:mysql://127.0.0.1:3306/test?user=root&password=123"
//     * @return
//     */
//    public static Connection getMysqlConnection(String connectionDesc) {
//        Connection conn = null;
//        PreparedStatement pstmt = null;
//        try {
//            long start = System.currentTimeMillis(); // 记录起始时间
//            LOG.info("mysql connection create start  \t -------------");
//            //加载驱动
//            Class.forName("com.mysql.jdbc.Driver");
//            //得到连接
//            conn = (Connection) DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?user=root&password=123");
//            if (!connectionCacheMap.containsKey(connectionDesc)) {
//                connectionCacheMap.put(connectionDesc, conn);
//            }
//            long end = System.currentTimeMillis(); // 记录起始时间
//            LOG.info("mysql connection create end : 耗时：" + String.valueOf(end - start) + "毫秒");
//            return conn;
//        } catch (ClassNotFoundException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    /**
//     * 得到mysql数据源
//     *
//     * @param connectionDesc 连接串 eg: "jdbc:mysql://127.0.0.1:3306/test?user=root&password=123"
//     * @param sql            eg:"select *  from student"
//     */
//    public static void getMysqlDataSource(String connectionDesc, String sql) {
//        Connection conn = null;
//        PreparedStatement pstmt = null;
//        try {
//            long start = System.currentTimeMillis(); // 记录起始时间
//            LOG.info("mysql execute start ----------");
//            if (DBUtils.connectionCacheMap.containsKey(connectionDesc)) {
//                conn = (com.mysql.jdbc.Connection) DBUtils.connectionCacheMap.get(connectionDesc);
//            } else {
//                conn = (com.mysql.jdbc.Connection) DBUtils.getMysqlConnection(connectionDesc);
//            }
//
//            if (DBUtils.preparedStatementCacheMap.containsKey(connectionDesc)) {
//                conn = (com.mysql.jdbc.Connection) DBUtils.preparedStatementCacheMap.get(connectionDesc);
//            } else {
//                conn = (com.mysql.jdbc.Connection) DBUtils.getMysqlConnection(connectionDesc);
//            }
//            pstmt = (com.mysql.jdbc.PreparedStatement) conn.prepareStatement(sql);
//            ResultSet rs = pstmt.executeQuery();
////            int col = rs.getMetaData().getColumnCount();
////            while (rs.next()) {
////                for (int i = 1; i <= col; i++) {
////                    System.out.print(rs.getString(i) + "\t");
////                    if ((i == 2) && (rs.getString(i).length() < 8)) {
////                        System.out.print("\t");
////                    }
////                }
////                System.out.println("");
////            }
//            pstmt.close();
//            long end = System.currentTimeMillis(); // 记录起始时间
//            LOG.info("mysql execute end : 耗时：" + String.valueOf(end - start) + "毫秒");
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }

    /**
     * 项目启动时，对所有连接进行缓存
     *
     * @param taskVo
     */
    public static void initOracleConnection(TaskVo taskVo) {
        //从db 读取所有的数据源连接。创建连接，
        try {
            DataSourceModel dataSourceModel = taskVo.getDataSource();
            TaskDetailModel taskDetailModel = taskVo.getTaskDetail();
            String cronTrigger = taskDetailModel.getCronExpression();
            LOG.info(String.format("--- InitOracleConnection dataSource start: url : %s ,username: %s, Password: %s ", dataSourceModel.getDataSourceUrl(), dataSourceModel.getUserName(), dataSourceModel.getPassword()));
            Long startTime = System.currentTimeMillis();
            for (String cron : cronTrigger.split("@")) {
                createOracleConnection(dataSourceModel.getDataSourceUrl(), dataSourceModel.getUserName(), dataSourceModel.getPassword(), taskDetailModel.getTaskGroupName(), taskDetailModel.getTaskName(), ScheduleUtils.trigerNameFormat(taskDetailModel.getTaskName(), cron));
            }
            Long endTime = System.currentTimeMillis();
            LOG.info(String.format("--- InitOracleConnection  dataSource url end: url: %s ,username: %s, Password: %s  ,cost time: %d", dataSourceModel.getDataSourceUrl(), dataSourceModel.getUserName(), dataSourceModel.getPassword(), endTime - startTime));
        } catch (Exception e) {
            throw new RuntimeException("Error  连接初始化失败");
        }
    }

    public static void initOracleConnection(String url, String userName, String password, String jobGroupName, String jobName, String cronTrigger) {
        //从db 读取所有的数据源连接。创建连接，
        try {
            LOG.info(String.format("--- InitOracleConnection dataSource start: url : %s ,username: %s, Password: %s ", url, userName, password));
            Long startTime = System.currentTimeMillis();
            for (String cron : cronTrigger.split("@")) {
                createOracleConnection(url, userName, password, jobGroupName, jobName, ScheduleUtils.trigerNameFormat(jobName, cron));
            }
            Long endTime = System.currentTimeMillis();
            LOG.info(String.format("--- InitOracleConnection  dataSource url end: url: %s ,username: %s, Password: %s  ,cost time: %d", url, userName, password, endTime - startTime));
        } catch (Exception e) {
            throw new RuntimeException("Error  连接初始化失败");
        }
    }


    /**
     * oracle 获取连接
     *
     * @param url      eg: "jdbc:oracle:thin:@localhost:1521:ORCL"
     * @param username eg:"demo"
     * @param password eg:"123"
     * @return
     */
    public static java.sql.Connection createOracleConnection(String url, String username, String password, String jobGroup, String jobName, String cronTrigger) {
        LOG.info(String.format("--- --- ---  Create  oracle connection start:url:%s,userName:%s,password:%s JobGroup: %s ,JobName: %s , cronTrigger:%s---", url, username, password, jobGroup, jobName, cronTrigger));
        java.sql.Connection conn = null;
        try {
            long start = System.currentTimeMillis(); // 记录起始时间
            //加载驱动
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //得到连接
            conn = (java.sql.Connection) DriverManager.getConnection(url, username, password);// DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?user=root&password=123");

            String connectionDesc = getConnnectionIdentity(url, username, password, jobGroup, jobName, cronTrigger);
            if (!connectionCacheMap.containsKey(connectionDesc)) {
                ConnectionObject connectionObject = new ConnectionObject();
                connectionObject.setConnection(conn);
                connectionObject.setIsUsed(false);
                connectionCacheMap.put(connectionDesc, connectionObject);
            }
            long end = System.currentTimeMillis(); // 记录终止时间
            LOG.info("--- --- ---  Create oracle connection  end , cost time：" + String.valueOf(end - start) + "msec ---");
            return conn;
        } catch (SQLException e) {
            LOG.info(e.getMessage());
        } catch (ClassNotFoundException e) {
            LOG.info(e.getMessage());
            LOG.info("--- ---- --- Create oracle connection fail,username or userpassword or url   may have error ---");
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                }
            }
        }
        return null;
    }

    public static java.sql.Connection getTmpConnection(String url, String username, String password, String jobGroup, String jobName, String cronTrigger) {
        LOG.info(String.format("--- --- ---  Create tmp oracle connection start:url:%s,userName:%s,password:%s JobGroup: %s ,JobName: %s ,%s cronTrigger:%s---", url, username, password, jobGroup, jobName, cronTrigger));
        java.sql.Connection conn = null;
        try {
            long start = System.currentTimeMillis(); // 记录起始时间
            //加载驱动
            Class.forName("oracle.jdbc.driver.OracleDriver");
            //得到连接
            conn = (java.sql.Connection) DriverManager.getConnection(url, username, password);// DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test?user=root&password=123");
            String connectionDesc = getConnnectionIdentity(url, username, password, jobGroup, jobName, cronTrigger);
            long end = System.currentTimeMillis(); // 记录终止时间
            LOG.info("--- --- ---  Create tmp oracle  connection  end , cost time：" + String.valueOf(end - start) + "msec ---");
            return conn;
        } catch (SQLException e) {
            LOG.info(e.getMessage());
        } catch (ClassNotFoundException e) {
            LOG.info(e.getMessage());
            LOG.info("--- ---- --- Create tmp oracle  connection fail,username or userpassword or url   may have error ---");
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e1) {
                }
            }
        }
        return null;
    }

    /**
     * 获取oracle数据源
     *
     * @param url      eg: "jdbc:oracle:thin:@localhost:1521:ORCL"
     * @param username eg:"demo"
     * @param password eg:"123"
     */
    public synchronized static java.sql.Connection getOracleConnection(String url, String username, String password, String jobGroup, String jobName, String cronTriggerTime) throws SQLException {

        LOG.info(String.format("--- Get Oracle Connection Start: JobGroup: %s ,JobName: %s ---", jobGroup, jobName));
        java.sql.Connection conn = null;
        long start = System.currentTimeMillis(); // 记录起始时间
        String connectionDesc = getConnnectionIdentity(url, username, password, jobGroup, jobName, cronTriggerTime);
        if (DBUtils.connectionCacheMap.containsKey(connectionDesc)) {
            LOG.info("--- --- Use Cache Oracle Connection ---");
            ConnectionObject connectionObject = (ConnectionObject) DBUtils.connectionCacheMap.get(connectionDesc);
            conn = connectionObject.getConnection();
            if (connectionObject.getIsUsed()) {
                LOG.info("task execute too quick ,please add time span ");
                return null;
            }
            connectionObject.setIsUsed(true);
            if (!isValid(conn)) {
                LOG.info("--- --- --- Connection cache is invalid ---");
                conn = DBUtils.createOracleConnection(url, username, password, jobGroup, jobName, cronTriggerTime);
                DBUtils.connectionCacheMap.remove(connectionDesc);
                ConnectionObject co = new ConnectionObject();
                co.setConnection(conn);
                co.setIsUsed(true);
                DBUtils.connectionCacheMap.put(connectionDesc, co);
            }
        } else {
            conn = DBUtils.createOracleConnection(url, username, password, jobGroup, jobName, cronTriggerTime);
            if (!isValid(conn)) {
                LOG.info(String.format("--- Craete oracle connection fail ---", jobGroup, jobName));
                return null;
            }
            ConnectionObject co = new ConnectionObject();
            co.setConnection(conn);
            co.setIsUsed(true);
            DBUtils.connectionCacheMap.put(connectionDesc, co);
        }
        LOG.info(String.format("--- Get oracle connection end: JobGroup: %s ,JobName: %s ---", jobGroup, jobName));
        return conn;
    }

    /**
     * 得到连接字符串的唯一标识符
     * url_username_password@groupName_jobname_trigger
     *
     * @param url
     * @param username
     * @param password
     * @param jobGroup
     * @param jobName
     * @param cronTriggerTime
     * @return
     */
    public static String getConnnectionIdentity(String url, String username, String password, String jobGroup, String jobName, String cronTriggerTime) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(url).append("_").append(username).append("_").append(password).append("&").append(ScheduleUtils.trigerNameFormat(jobGroup + "_" + jobName, cronTriggerTime));
        return stringBuffer.toString();
    }

    public static String getConnnectionIdentity(String url, String userName, String password, String groupName, String formatTrigger) {
        return url + "_" + userName + "_" + password + "&" + groupName + "_" + formatTrigger;
    }

    /**
     * 根据DataSource，得到连接前缀
     *
     * @param url
     * @param username
     * @param password
     * @return
     */
    public static String getConnectionDescPrefix(String url, String username, String password) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(url).append("_").append(username).append("_").append(password);
        return stringBuffer.toString();
    }

    /**
     * 释放连接，置标志位为false
     *
     * @param url
     * @param username
     * @param password
     * @param jobGroup
     * @param jobName
     * @param cronTriggerTime
     * @return
     */
    public static boolean releaseConn(String url, String username, String password, String jobGroup, String jobName, String cronTriggerTime) {
        String connectionDesc = getConnnectionIdentity(url, username, password, jobGroup, jobName, cronTriggerTime);
        if (DBUtils.connectionCacheMap.containsKey(connectionDesc)) {
            LOG.info("--- --- realease Cache Oracle Connection start ---");
            ConnectionObject connectionObject = (ConnectionObject) DBUtils.connectionCacheMap.get(connectionDesc);
            connectionObject.setIsUsed(false);
            LOG.info("--- --- realease Cache Oracle Connection  end ---");
            return true;
        } else {
            //throw new RuntimeException("--- cache oracle connection lose ");
            return false;
        }
    }

    /**
     * 执行SQL
     *
     * @param conn
     * @param sql
     * @return
     */
    public static Map<String, Object> executeOracleSelect(java.sql.Connection conn, String sql) {
        Map<String, Object> rsMap = new HashMap<String, Object>();
        OraclePreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            long start = System.currentTimeMillis(); // 记录起始时间
            LOG.info(String.format(" Oracle execute sql: %s  start ----------", sql));
            pstmt = (OraclePreparedStatement) conn.prepareStatement(sql, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            rsMap.put("ps", pstmt);
            rs = pstmt.executeQuery();
            rsMap.put("rs", rs);
            int col = rs.getMetaData().getColumnCount();
            long end = System.currentTimeMillis(); // 记录起始时间
            LOG.info(String.format("--- Oracle execute sql: %s end : cost time：%d msec", sql, end - start));
            return rsMap;
        } catch (SQLException e) {
            if (null != pstmt)
                try {
                    pstmt.close();
                } catch (SQLException e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            return null;
        }
    }


    // 判断连接是否可用
    private static boolean isValid(java.sql.Connection conn) {
        try {
            if (conn == null || conn.isClosed()) {
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * @param connectionDescMap:url,username,password,groupname,jobname,crontrigger
     *
     * @return
     */
    public static boolean deleteConncetionCache(Map<String, String> connectionDescMap) {
        String connectionIdentity = getConnnectionIdentity(connectionDescMap.get("url"), connectionDescMap.get("username"), connectionDescMap.get("password"), connectionDescMap.get("groupname"), connectionDescMap.get("cron"));
        if (DBUtils.connectionCacheMap.containsKey(connectionIdentity)) {
            ConnectionObject connectionObject = DBUtils.connectionCacheMap.get(connectionIdentity);
            try {
                if (connectionObject.getConnection() != null && !connectionObject.getIsUsed() && !connectionObject.getConnection().isClosed())
                    connectionObject.getConnection().close();
                connectionCacheMap.remove(connectionIdentity);
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            LOG.info("ERROR: delete connection fail ,connection not exist");
            return false;
        }
        return true;
    }

    public static boolean  deleteConnectionCache(String connectionIdentity) {
        if (DBUtils.connectionCacheMap.containsKey(connectionIdentity)) {
            ConnectionObject connectionObject = DBUtils.connectionCacheMap.get(connectionIdentity);
            try {
                if (connectionObject.getConnection() != null && !connectionObject.getIsUsed() && !connectionObject.getConnection().isClosed())
                    connectionObject.getConnection().close();
                connectionCacheMap.remove(connectionIdentity);
            } catch (SQLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        } else {
            LOG.info("ERROR: delete connection fail ,connection not exist");
            return false;
        }
        return true;
    }

    public static boolean deleteConnectionCache(DataSourceModel dataSourceModel) {
        Iterator entries = DBUtils.connectionCacheMap.entrySet().iterator();
        String connDescPrefix = DBUtils.getConnectionDescPrefix(dataSourceModel.getDataSourceUrl(), dataSourceModel.getUserName(), dataSourceModel.getPassword());
        while (entries.hasNext()) {
            try {
                Map.Entry entry = (Map.Entry) entries.next();
                String connecionDesc = (String) entry.getKey();
                if (connecionDesc.matches(String.format("^%s(.*)", connDescPrefix))) {
                    ConnectionObject connectionObject = (ConnectionObject) entry.getValue();
                    if (connectionObject.getConnection() != null && !connectionObject.getIsUsed() && !connectionObject.getConnection().isClosed())
                        connectionObject.getConnection().close();
                    entries.remove();
                }
            } catch (SQLException ex) {
                LOG.info("删除连接缓存失败:" + ex.getMessage());
            }

        }
        return false;
    }



    //test
    public static ResultSet getOracleConnection(String url, String username, String password, String sql) {
        return null;
    }



}
