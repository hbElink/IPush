<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://"
      + request.getServerName() + ":" + request.getServerPort()
      + path + "/";

%>
<!DOCTYPE html>
<html>
<head>
    <base href="<%=basePath%>">
  <title>首页</title>

    <!--NOTE this file can NOT pack, if URI changed, also change main.js line 94-->
  <link href="<%= basePath %>css/vendor.css" rel="stylesheet">
  <link href="<%= basePath %>css/react-bootstrap-treeview.css" rel="stylesheet">
  <link href="<%= basePath %>css/style.css" rel="stylesheet">
  <link href="<%= basePath %>css/dropzone.css" rel="stylesheet">
  <link rel="stylesheet" href="/css/treeview.css"/>
    <%--<link type="text/css" rel="stylesheet" href="<%= basePath %>js/dist/themes/style1/orange-blue.css" id="theme-change" class="style-change color-change">--%>


    <script type="text/javascript">
        function getPath(){

            return "<%=basePath%>";
        }
        function getUserName(){
            return "${userInfo.userRealname}";
        }
    </script>
</head>
<body>
  <script src="<%=basePath%>js/dist/third/jquery.js"></script>
  <script src="<%=basePath%>js/dist/third/JSXTransformer.js"></script>
  <script src="<%=basePath%>js/dist/React-Components-0.1.4.min.js"></script>
  <!--<script src="http://127.0.0.1:3000/static/bundle.js"></script>-->
  <!-- 路由配置文件 -->
  <script type="text/javascript" src="<%=basePath%>js/pages/config.js"></script>
  
</body>
</html>
