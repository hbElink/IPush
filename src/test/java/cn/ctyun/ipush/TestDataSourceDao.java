package cn.ctyun.ipush;

import cn.ctyun.ipush.dao.DataSourceDao;
import cn.ctyun.ipush.model.DataSourceModel;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-18
 * Time: 下午1:26
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config_test.xml"})
@Transactional
public class TestDataSourceDao {

    @Autowired
    private DataSourceDao dataSourceDao;

    @Test
    @Rollback(false)
    public void testSave(){
        DataSourceModel dataSoureModel=new DataSourceModel();
        dataSoureModel.setExcelListDb("123");
        dataSoureModel.setDataSourceModelId("12312123132123");
        dataSourceDao.save(dataSoureModel);
    }

    @Test
    @Rollback(false)
    public void testQuerySinglerResult(){
       DataSourceModel dataSourceModel=  dataSourceDao.querySingleResult("12312123132123");
        System.out.println("--------------------Rusult start-----------------");
        System.out.println(JSON.toJSON(dataSourceModel));
        System.out.println("--------------------Rusult end-----------------");
    }


    @Test
    @Rollback(false)
    public void testUpdate(){
        DataSourceModel dataSoureModel=new DataSourceModel();
        dataSoureModel.setExcelListDb("123123123");
        dataSoureModel.setDataSourceModelId("12312123132123");
        int updateCount=  dataSourceDao.update(dataSoureModel);
        testQuerySinglerResult();
    }

    @Test
    @Rollback(false)
    public void testDelete(){
        int updateCount=  dataSourceDao.delete("12312123132123");
        testQuerySinglerResult();
    }

    @Test
    @Rollback(false)
    public void testQueryList(){
        List<DataSourceModel> dataSourceModelList=  dataSourceDao.queryList(new DataSourceModel());
        System.out.println("--------------------Rusult start-----------------");
        for (DataSourceModel dsm:dataSourceModelList)
        System.out.println(JSON.toJSON(dsm));
        System.out.println("--------------------Rusult end-----------------");
    }
}
