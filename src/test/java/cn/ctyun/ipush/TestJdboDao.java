package cn.ctyun.ipush;

import cn.ctyun.ipush.dto.TaskDsDto;
import cn.ctyun.ipush.model.ScheduleJob;
import cn.ctyun.ipush.model.TaskModel;
import com.alibaba.fastjson.JSON;
import com.dexcoder.commons.pager.Pager;
import com.dexcoder.commons.utils.UUIDUtils;
import com.dexcoder.dal.JdbcDao;
import com.dexcoder.dal.build.Criteria;
import com.dexcoder.dal.spring.page.PageControl;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-12
 * Time: 上午11:33
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-config_test.xml"})
@Transactional
public class TestJdboDao {

    /** 通用dao */
    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private PageControl pageControl;

    private ScheduleJob scheduleJob;

    @Test
    public void QueryList(){

        List<ScheduleJob> scheduleJobList = jdbcDao.queryList(Criteria.select(ScheduleJob.class));
        Assert.assertEquals(1,1);
    }

    @Test
    @Rollback(false)
    public void queryCount(){
        scheduleJob=new ScheduleJob();
        scheduleJob.setJobGroup("sjz");
        scheduleJob.setJobName("zh1");
        int count = jdbcDao.queryCount(scheduleJob);
        Assert.assertEquals(1,count);
    }

    @Test
    @Rollback(false)
    public void testcriteria(){

        //test select
       // Criteria criteria =Criteria.select(TaskModel.class).exclude("dataSource","taskDetailModel","excel","picture","email","yixin").where("taskModelId",new Object[]{"1"});
       // TaskModel tm=jdbcDao.querySingleResult(criteria);

        //test insert
       // Criteria criteria11=Criteria.insert(TaskModel.class).set("TaskModelId", UUIDUtils.getUUID32()).set("taskDetailModelId","2");
//        TaskModel tm=new TaskModel();
//        tm.setTaskModelId(UUIDUtils.getUUID32());
//        tm.setDataSourceId(UUIDUtils.getUUID32());
//        tm.setEmailId(UUIDUtils.getUUID32());
//        tm.setExcelId(UUIDUtils.getUUID32());
//        jdbcDao.save(tm);

//        TaskModel tm=new TaskModel();
//        tm.setTaskModelId("679ab1b0a34240b094d6d49ed3beb838");
//        tm=jdbcDao.querySingleResult(tm);
//        tm.setExcelId("1");
//        jdbcDao.update(tm);

         //int count=  jdbcDao.updateForSql("update schedule_job set is_deleted = 0");
        Criteria criteria=Criteria.update(TaskModel.class).set("isDeleted","2").where("isDeleted","= ",new Object[]{"0"});
        int count=jdbcDao.update(criteria);
        Assert.assertEquals(1,1);

    }

    /**
     * 测试jdbcDao事务
     * 缓存策略及默认的回滚机制，使数据库记录不会真的发生改变
     */
    @Test
    public void testTransaction(){

        ScheduleJob scheduleJob1=new ScheduleJob();
        scheduleJob1.setJobGroup("sjz");
        ScheduleJob scheduleJobRs = jdbcDao.querySingleResult(scheduleJob1);
        scheduleJobRs.setJobName("zh7");
        jdbcDao.update(scheduleJobRs);

        ScheduleJob scheduleJobRs2 = jdbcDao.querySingleResult(scheduleJob1);
        String jobname=scheduleJobRs2.getJobName();
        try{
            TaskModel tm=new TaskModel();
            tm.setTaskModelId("679ab1b0a34240b094d6d49ed3beb838");
            tm=jdbcDao.querySingleResult(tm);
            tm.setExcelId("7");
            jdbcDao.update(tm);
        }catch (Exception ex){

        }
    }

    /**
     *测试分页
     */
    @Test
    public void testPerformPage(){
        //分页查询
        pageControl.performPage(1, 2);
        Criteria criteria = Criteria.select(TaskModel.class);
        List<TaskModel> tm= jdbcDao.queryList(criteria);
        List<TaskModel> users = pageControl.getPager().getList(TaskModel.class);
        for (TaskModel us : users) {
            System.out.println(us.getDataSourceId());
        }
    }

    @Test
    public void testMultTb(){
          //map方式获取数据 不需要构造对象实体 单数get的时候需要知道名称 容易写错
//        TaskDsDto taskDsDto = new TaskDsDto();
//        List<Map<String, Object>> mapList = jdbcDao.queryListForSql("cn.ctyun.ipush.dto.TaskDsDto.getData");
//        for (Map<String, Object> map : mapList) {
//            System.out.println(map.get("userId"));
//            System.out.println(map.get("loginName"));
//        }
        // 对象实体的方式获取数据 阅读方便 推荐使用这种
//        List<TaskDsDto> taskDsDto2=jdbcDao.queryListForSql("cn.ctyun.ipush.dto.TaskDsDto.getData",TaskDsDto.class);
//        for (TaskDsDto tdd : taskDsDto2) {
//           System.out.print(tdd.toString());
//        }

        // 对象实体的方式获取数据 阅读方便 推荐使用这种 附加参数类型
        TaskDsDto taskDsDto3 = new TaskDsDto();
        taskDsDto3.setDataSourceId("123");
        Object[] names = new Object[] { "aaa", "selfly_a94", "selfly_a95" };
        List<TaskDsDto> taskDsDto4=jdbcDao.queryListForSql("cn.ctyun.ipush.dto.TaskDsDto.getData","params",new Object[] { taskDsDto3, names },TaskDsDto.class);
        for (TaskDsDto tdd : taskDsDto4) {
            System.out.print(tdd.toString());
        }
    }



}
