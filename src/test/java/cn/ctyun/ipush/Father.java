package cn.ctyun.ipush;

/**
 * Created with IntelliJ IDEA.
 * User: user
 * Date: 16-6-14
 * Time: 上午10:20
 * To change this template use File | Settings | File Templates.
 */
public class Father {
    private String fid;
    private String fname;

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }
}
